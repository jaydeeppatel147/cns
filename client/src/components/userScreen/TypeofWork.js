import React, { Component, useState, useEffect, useContext } from "react";
import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import UserFooter from './UserFooter'
import Fade from "react-reveal/Fade";
import Modal from "react-modal";
import Zoom from "react-reveal/Zoom";
import NavBar from "../Navbar";

const TypeofWork = () => {
  return (
    <div>
      <NavBar></NavBar>
      <div className="our-work">

        {/* sub heading */}
        <div className="work-heading w-container">
          <div className="h1-div">
            <h1 className="h1-work">Work</h1>
          </div>
          <div className="project-div">
            <Link to='/user-work' style={{ textDecoration: 'none' }}>   <div className="type-of-work">ALL PROJECTS</div></Link>
            <Link to='/user-type-of-work' style={{ textDecoration: 'none' }}> <div class="project">type of work</div></Link>

          </div>

        </div>

        {/* img grid */}
        <div className="our-work-content w-container">
          <Zoom bottom cascade>
            <div className="work-column w-row">

              <div className="work-col w-col w-col-4">
                <div className="work-col-div">
                  <Link to='/user-branding' style={{ textDecoration: 'none' }} >
                    <img src="images/Website_thumbnails-01_1.png" loading="lazy" alt="" className="work-img" />
                  </Link>

                  {/* <div className="text-block-8"><span className="text-span-2">Upciclo</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>
              <div className="work-col w-col w-col-4">

                <div className="work-col-div">
                  <Link to='/user-dm' style={{ textDecoration: 'none' }} >
                    <img src="images/Website_thumbnails-02_1.png" loading="lazy" alt="" className="work-img" />
                  </Link>
                  {/* <div className="text-block-8"><span className="text-span-2">Agromatic</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>
              <div className="work-col w-col w-col-4">
                <div className="work-col-div">
                  <Link to='/user-wd' style={{ textDecoration: 'none' }} >
                    <img src="images/Website_thumbnails-03_1.png" loading="lazy" alt="" className="work-img" />
                  </Link>
                  {/* <div className="text-block-8"><span className="text-span-2">Unimech</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>

            </div>
          </Zoom>
          <Zoom bottom cascade>
            <div className="work-column w-row">

              <div className="work-col w-col w-col-4">
                <div className="work-col-div">
                  <Link to='/user-pv' style={{ textDecoration: 'none' }} >
                    <img src="images/tow4.jpg" loading="lazy" alt="" className="work-img" />
                  </Link>

                  {/* <div className="text-block-8"><span className="text-span-2">Upciclo</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>
              <div className="work-col w-col w-col-4">

                <div className="work-col-div">
                  <Link to='/user-socialmedia' style={{ textDecoration: 'none' }} >
                    <img src="images/socialMedia.jpg" loading="lazy" alt="" className="work-img" />
                  </Link>
                  {/* <div className="text-block-8"><span className="text-span-2">Agromatic</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>
              <div className="work-col w-col w-col-4">
                <div className="work-col-div">
                  <Link to='/user-he' style={{ textDecoration: 'none' }} >
                    <img src="images/he.jpg" loading="lazy" alt="" className="work-img" />
                  </Link>
                  {/* <div className="text-block-8"><span className="text-span-2">Unimech</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>

            </div>
          </Zoom>

          <Zoom bottom cascade>
            <div className="work-column w-row">

              <div className="work-col w-col w-col-4">
                <div className="work-col-div">
                  <Link to='/user-animation' style={{ textDecoration: 'none' }} >
                    <img src="images/animation.jpg" loading="lazy" alt="" className="work-img" />
                  </Link>

                  {/* <div className="text-block-8"><span className="text-span-2">Upciclo</span> - We helped Upciclo get on</div> */}
                  {/* <div className="text-block-9">social media and do mass campaigns</div> */}
                </div>
              </div>
              
              
            </div>
          </Zoom>
          
        </div>

        <UserFooter></UserFooter>
      </div>
    </div>
  )
}

export default TypeofWork

