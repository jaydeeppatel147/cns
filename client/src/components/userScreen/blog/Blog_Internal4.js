import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Internal = () => {
  return (
    <div>
    <NavBar></NavBar><br></br><br></br>
    <div data-ix="page-loading" className="page-wrappersidebar-headerh1">
      <div className="post-header-sectionh1">
        <div className="wrapperh1 w-containerh1">
          <div className="post-header-wrapper">
            <a href="#" data-ix="fade-up-1" className="categoryh1">
              {/* DESIGN */}
            </a>
            <h1 data-ix="fade-up-2">
              {/* When you are down and out, how to <br /> */}
              {/* keep going */}
            </h1>
            <a
              href="#"
              data-ix="fade-up-3"
              className="post-header-authorh1 w-inline-blockh1"
            >
              <div>Post by</div>
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-header-avatarh1"
              />
              <div>Divyansh</div>
            </a>
          </div>
        </div>
        <div className="post-header-overlayh1"></div>
      </div>
      <div className="sectionh1">
        <div className="wrapper w-containerh1">
          <div className="blog-post-contenth1">
            <div className="blog-post w-richtexth1">
              <h2>
                <b>Moment Marketing & how it improves audience engagement</b>
              </h2>
              <br></br>
              <p>
                RASODE MEIN KON THA? MAIN THI TUM THI KON THA? This question is
                going viral, breaking all social media records. Brands like
                parle, zomato, Britannia goodday and many more are using this as
                an opportunity to market their products
              </p>
              <br />
              <h3>Put an empty cooker on the gas again?</h3>
              <br></br>

              <h3>Just relax and snack on parle instead#Rashi.</h3>
              <br></br>
              <h3>
                Embracing the moment and creating a campaign relating to it is
                popularly known as Moment Marketing
              </h3>
              <br />
              <p>
                <span>
                  <b>DIGITAL MARKETING</b>
                </span>{" "}
                is always evolving and moment marketing is something that has
                recently come into force. Moment is a very brief period of time,
                and using that opportunity to market your product in an
                effective way is what it is all about. It gives you the
                opportunity to connect with viewers, both online and offline in
                real time. It is one of the most effective ways to keep the
                handles engaged, at a marginal cost. Most advertising agencies
                resort to this, when creating content for their social media
                accounts. With Moment Marketing, when a consumer scrolls through
                an advertisement that has reference to a trending topic, not
                only does it capture the viewer’s attention but also makes them
                remember it for a longer time. In the age of digital marketing,
                thumb stopping (something that stops audiences to stop scrolling
                the screen for the content piece) has become the new barometer
                of success.
              </p>
              <br></br>
              <p>
                With all the insights about the customers available, why not
                make the most out of it and present the product at the right
                time to the right audience through the right platform? I mea,
                why let any opportunity pass, when you can make the most out of
                it?
              </p>
              <br />
              <p>
                However, one needs to be careful when using this digital
                marketing technique. There is a very thin line between a funny
                post and an offensive post, and if you cross that line, it could
                backfire. With success comes the risk of failure, your campaign
                may turn out to be totally disastrous, or it may be a huge
                success. But, you have to take that risk. It is similar to
                drinking wine, it may taste different depending upon the
                conditions in which it is drunk, with light and sound impacting
                on how fresh, fruity, bitter or acidic the drink is perceived.
              </p>
              <br />

              <p>
                People are bored of conventional ads now, and they want
                something new. As an advertising agency, you have to keep up
                with your customers' desires. In the era of digital marketing,
                when the industry has reached a point of "content shock" where
                consumers cannot consume more content than they already do, it
                has become extremely difficult for advertising agencies to
                attract their customers digitally. This is where moment
                marketing comes into play. When you advertise products keeping
                in mid current trends, it gives an impression to your audience
                that you are an active and witty brand, and most importantly it
                enhances your overall perception in people’s mind.
              </p>
              <br />
              <p>
                The timing and the content are very important aspects of moment
                marketing. If you do it late, there are many examples out there,
                and the moment has passed. If the content isn’t engaging, the
                chance to make an impact is lost. Brand marketers should always
                be on the lookout for such opportunities & act fast and strong.
                The digital marketing agencies should keep in mind that when you
                have access to all the insights, it means every agency does too
                and so, timing is very important.
              </p>

              <br />

              <h4>There can be 4 different types of moment marketing</h4>
              <br></br>
              <ol>
                <li>
                  {" "}
                  Want to know moments- when you are listening to a song and
                  want to know the name you search for it. You would Google it
                  and get the information on your fingertips.
                </li>
                <br />
                <li>
                  {" "}
                  Want to go moments- these are the moments where one searches
                  for the nearby location for having meals, shopping and
                  tourist’s attractions.
                </li>
                <br />
                <li>
                  Want to buy moments- people want to make online purchases so
                  they search for it and can make a decision immediately and
                  purchase it.
                </li>
              </ol>
              <br />
              <p>
                From food delivery apps to FMCG brands and government
                organisations, everyone uses moment marketing under digital
                marketing to grab the consumer’s mind space. Amul being a case
                in point – most moment marketing examples are tactical in
                nature, and are used to increase brand salience by leveraging a
                narrow window of opportunity. It helps make a brand become more
                noticeable by associating it with a topic during the period that
                the particular topic is trending. It can be highly effective
                when the topic has direct relevance to a brand or a product
                category. The Paytm ad that appeared the day after the sudden
                demonetisation announcement is a good example.
              </p>
              <br />
              <p>
                #SabkoChabaJaenge, a digital campaign created by Dabur India
                Ltd. to promote its brand Red Toothpaste during the 2019 Cricket
                World Cup, was a successful campaign.
              </p>
              <br />
              <p>
                The series of ads garnered a huge 28 million views across
                digital platforms. Apart from this, Amul has been doing moment
                marketing for many years. The Company believes that once in a
                while engagement won’t work, and you have to commit to long-term
                engagement to associate the brand and services to the consumers.
                They promote themselves through moment marketing, not just on
                their social media handles, but also on offline mediums like
                newspapers, thereby allowing their audience to engage with them
                at every moment. Here are a few examples of it:
              </p>
              <br />

              <p>
                The entire Amul campaign is built around moment marketing, 100%
                of spends of Amul goes into moment marketing. For other brands,
                moment marketing is part of the campaign but for Amul it is the
                campaign
              </p>
              <br />

              <p>
                Having said this, it is also important for brands to say things
                that are true to what they stand for, instead of only talking
                about trends. Because if all you talk about are trends,
                consumers will fail to associate you strongly with one
                particular product, service or cause. moment marketing under
                digital marketing is something that can level up your game but
                it certainly does not mean that static advertisements can
                completely be ignored.
              </p>
            </div>
            <div className="post-date-wrapperh1">
              <div>Published</div>
              <div className="post-dateh1">on 5th Dec</div>
            </div>
            <div className="post-author-profileh1">
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-author-avatarh1"
              />
              <div className="post-author-infoh1">
                <h5 className="post-author-nameh1">Divyansh</h5>
                <div className="post-author-bioh1">
                  This is some text inside of a div block.
                </div>
                <a href="#" className="post-author-linkh1">
                  View All Posts
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="section grey-sectionh1">
        <div className="section-header-wrapperh1">
          <div className="section-small-headerh1">More From Blog</div>
          <h2 className="section-headerh1">You Might Also Like</h2>
        </div>
        <div className="wrapper w-containerh1">
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
        </div>
      </div> */}
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Internal;
