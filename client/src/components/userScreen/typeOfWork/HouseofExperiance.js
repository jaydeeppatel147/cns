import React from 'react'

import NavBar from "../../Navbar";
import UserFooter from '../UserFooter'

const HouseofExperiance = () => {
    return (
        <div>
            <NavBar></NavBar><br></br>
            <div className="branding-2">

                {/* main img */}
                {/* <div className="branding-img">
                    <img src="images/branding1.jpg" loading="lazy" sizes="(max-width: 1920px) 100vw, 1920px"  alt="" /><div className="center">srhsrh</div>
                </div> */}

                <div class="containerImg">
                    <img src="images/branding1.jpg" alt="Snow" sizes="(max-width: 1920px) 100vw, 1920px" />

                    <div class="centeredImg">

                        <div className="branding2">
                            <br></br><br></br>
                            <b className="textImg">HOUSE OF EXPERIENCE</b>
                        </div>


                    </div>
                </div>

                <div className="branding-container w-container">
                    <div className="w-layout-grid grid-12">
                        <div>
                            <h1 className="heading-24">What we do</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">Services</h2>
                            <p className="paragraph-5">We create experiences that enhance your brand’s recall among your target audience.We ideate online & offline strategies which will increase your brand’s presence & credibility. 
 </p>
                            <h2 className="heading-25">Roles &amp; responsibility</h2>
                            <p className="paragraph-5">From brainstorming & developing your brand’s identity, to hand holding it when it walks through the market, we have got you covered. We do our research, and ensure that your brand stands out from the crowd.

</p>
                        </div>
                        <div>
                            <h1 className="heading-24">Process</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">How we do things</h2>
                            <p className="paragraph-5"> We first do our research on current trends related to the nature of your brand/business. Our next step is to come up with a unique strategy which will increase the presence of your brand both online & offline. Then, we start implementing our revolutionary ideas which will wow your audience & clients too. 
</p>
                        </div>
                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default HouseofExperiance