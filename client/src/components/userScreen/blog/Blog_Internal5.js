import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Internal = () => {
  return (
    <div>
    <NavBar></NavBar><br></br><br></br>
    <div data-ix="page-loading" className="page-wrappersidebar-headerh1">
      <div className="post-header-sectionh1">
        <div className="wrapperh1 w-containerh1">
          <div className="post-header-wrapper">
            <a href="#" data-ix="fade-up-1" className="categoryh1">
              {/* DESIGN */}
            </a>
            <h1 data-ix="fade-up-2">
              {/* When you are down and out, how to <br /> */}
              {/* keep going */}
            </h1>
            <a
              href="#"
              data-ix="fade-up-3"
              className="post-header-authorh1 w-inline-blockh1"
            >
              <div>Post by</div>
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-header-avatarh1"
              />
              <div>Divyansh</div>
            </a>
          </div>
        </div>
        <div className="post-header-overlayh1"></div>
      </div>
      <div className="sectionh1">
        <div className="wrapper w-containerh1">
          <div className="blog-post-contenth1">
            <div className="blog-post w-richtexth1">
              <h2>
                <b>A guide to influencer marketing</b>
              </h2>
              <br />
              <p>
                If you seek a way to grab the attention of your customers, you
                shouldn’t look beyond someone who already has their attention.
                In short, you need influencers who can convey the image of your
                brand to the larger public of your potential target groups.
              </p>
              <br />
              <p>
                Any brand that uses endorsements and product placements from
                influencers is applying the influencer marketing tactic. The
                collaboration between a brand and an influencer is what
                indirectly helps the brand expand its community. An influencer
                does not necessarily mean a celebrity; he/she can be any one the
                following:
              </p>
              <br />
              <ol>
                <li>A person who has expertise in the field.</li>
                <li>A blogger who may have thousands of followers.</li>
                <li>A person with a niche audience</li>
                <li>A celebrity</li>
                <li>A social media influencer</li>
              </ol>
              <br />

              <p>
                Brands no longer have the centre stage; consumers do. To make it
                more interesting for the audience, there has been a shift from
                faceless sales executive to an authentic, familiar voice. A
                successful influencer can build authority, drive traffic to the
                sites, increase brand exposure and help connect with new
                audience groups.
              </p>
              <br />

              <h3>
                <b>Benefits of influencer marketing -</b>
              </h3>
              <br />
              <ul>
                <li>
                  Possibility for high ROI - The influencer chosen by the brand
                  has an audience that aligns with your product, therefore
                  having a high chance of conversion, which leads to higher
                  return on investment.
                </li>
                <br />
                <li>
                  Higher reach - When the influencer recommends or mentions your
                  product, it increases the brand’s visibility. With an
                  interesting enough creative approach, more people might share
                  it with their friends and your content can go viral, which
                  results in higher reach.
                </li>
                <br />
                <li>
                  Build trust and credibility - People follow certain
                  influencers who they find interesting and relevant, so any
                  recommendation from them is trustworthy and credible. In some
                  cases, people are persuaded to buy whatever the influencer
                  tells them to.
                </li>
                <br />

                <li>
                  Beats advertising blindness - People hate ads, they don’t like
                  to be interrupted. However, when they follow certain
                  influencers, they like hearing things from them and want to
                  try the products that they suggest. This subsequently helps
                  the brands, as they don’t have to push their product onto
                  people’s faces, but the influencer pulls the audience towards
                  them.
                </li>
                <br></br>
                <li>
                  Cheaper and more effective - Keeping celebrities aside, you
                  pay a certain amount to the influencer who can produce
                  similar, or sometimes better results. Most advertising tactics
                  put a limit to your reach, but not with influencer marketing.
                  It has the potential to be shared with a larger group than you
                  expected.
                </li>
                <br />
                <li>
                  Enriches your content strategy - When you partner your brand
                  with an influencer, both the influencer and the brand can
                  co-create the content. If you fall short of ideas they can
                  often help you, as they know what will attract users’
                  attention.
                </li>
              </ul>

              <br />
              <p>
                Most of the success from your influencer marketing campaign
                depends on the choice of influencers. If you make the right
                selection, the rewards are immense. Here is what you have to
                consider while choosing an influencer:
              </p>

              <ol>
                <li>
                  LOYAL FOLLOWERS - In order to advertise on an influencer
                  marketing platform, it is not necessary to be a big brand. You
                  don’t need influencers like Priyanka Chopra or Virat Kohli for
                  your brand. The important metric to consider is follower
                  loyalty. If you choose a person who has millions of followers
                  but very few likes and comments, it means their followers
                  aren’t loyal. When such an influencer says “buy this product
                  or use this service,” very few will listen. However, if you
                  choose a person who has loyal followers who genuinely show
                  interest in what they say, there’s a higher chance of
                  conversion. Always choose loyal following over larger
                  following numbers when it comes to influencers. Do not decide
                  based on how much they charge, but purely on the engagement
                  they are getting.
                </li>
                <br />
                <li>
                  RELATABLE - Choose people who are in tune with your brand.
                  Mapping out the relevant context for your brand is essential
                  to improve relevance in the target audience. A makeup brand
                  looking for an influencer to create content for their social
                  platforms must ensure that the influencer’s visual look suits
                  their brand guidelines appropriately. A footballer cannot do
                  it, the influencer has to be someone who already has an
                  expertise in that field, and whom people look up to.
                </li>
                <br />
                <li>
                  RESEARCH - It’s not just about choosing the best influencer,
                  but it is essential to research about the person’s likes and
                  dislikes. If an influencer has already mentioned that he/she
                  doesn’t like chocolates, it is not accurate to take them as an
                  influencer for a chocolate brand.
                </li>
                <br />

                <br />
              </ol>
              <br />

              <h3>
                <b>
                  Success parameters to check whether influencer marketing is
                  working for you:
                </b>
              </h3>

              <ol>
                <li>
                  Website traffic - It is the most important KPI (key
                  performance indicator) that can determine the success of your
                  campaign. Measure the number of new users, how much time they
                  spend on the site, visits from referrals and total page
                  reviews.
                </li>
                <br />
                <li>
                  Engagement rates - Engagement is the actual interaction your
                  content gets. To measure influencer engagement rates, keep a
                  track of clicks, likes, shares, comments, reactions and brand
                  mentions.
                </li>
                <li>
                  Conversion rates - It’s not enough that people are visiting
                  your site, it is important how many are actually converting
                  into sales. Keep a track of all the conversions from your
                  influencer engagement.
                </li>
                <br />
                <li>
                  Sales - It is the basic yet the most important KPI of all. If
                  your sales aren’t increasing, all of it will go in vain. There
                  are multiple ways to increase the sales - such as discount
                  coupons, referral links, promo codes and more.
                </li>
              </ol>
              <br />
              <p>
                We live in a time where choices are abundant and time is sparse.
                All the advertisements out there aren’t getting your customers
                attention. People want true stories from authentic people, for
                which influencers are the perfect choice. Influencer marketing
                opens up endless opportunities for brands to amplify their
                content, connect with customers and build relationships more
                organically. Always make sure to select the right influencers
                for your brand and focus on building long term relationships
                with them.
              </p>
            </div>
            <div className="post-date-wrapperh1">
              <div>Published</div>
              <div className="post-dateh1">on 5th Dec</div>
            </div>
            <div className="post-author-profileh1">
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-author-avatarh1"
              />
              <div className="post-author-infoh1">
                <h5 className="post-author-nameh1">Divyansh</h5>
                <div className="post-author-bioh1">
                  This is some text inside of a div block.
                </div>
                <a href="#" className="post-author-linkh1">
                  View All Posts
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="section grey-sectionh1">
        <div className="section-header-wrapperh1">
          <div className="section-small-headerh1">More From Blog</div>
          <h2 className="section-headerh1">You Might Also Like</h2>
        </div>
        <div className="wrapper w-containerh1">
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
        </div>
      </div> */}
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Internal;
