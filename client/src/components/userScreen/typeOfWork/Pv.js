import React from 'react'

import NavBar from "../../Navbar";
import UserFooter from '../UserFooter'

const Branding = () => {
    return (
        <div>
            <NavBar></NavBar><br></br>
            <div className="branding-2">

                {/* main img */}
                {/* <div className="branding-img">
                    <img src="images/branding1.jpg" loading="lazy" sizes="(max-width: 1920px) 100vw, 1920px"  alt="" /><div className="center">srhsrh</div>
                </div> */}

                <div class="containerImg">
                    <img src="images/branding1.jpg" alt="Snow" sizes="(max-width: 1920px) 100vw, 1920px" />

                    <div class="centeredImg">

                        <div className="branding2">
                            <br></br><br></br>
                            <b className="textImg">Photography And Videography</b>
                        </div>


                    </div>
                </div>

                <div className="branding-container w-container">
                    <div className="w-layout-grid grid-12">
                        <div>
                            <h1 className="heading-24">What we do</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">Services Live shoots-</h2>
                            <p className="paragraph-5">We provide professional photography shoots and videography services too. Along with these, we edit & package the videos/photos to bring out your business’ features exactly the way you want it.
 </p>
                            <h2 className="heading-25">Roles &amp; responsibility</h2>
                            <p className="paragraph-5">CNS takes care of all your live shoots and ensures that before we start processing them, your ideas are clearly understood and reproduced.
</p>
                        </div>
                        <div>
                            <h1 className="heading-24">Process</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">How we do things</h2>
                            <p className="paragraph-5"> We give you a timeline for the shoots & the editing schedule. Once this has been fixed, we keep you updated about how we plan to go about the shoot itself. After the completion of the shoot, we will go about the production in such a way that keeps up with modern trending consumer tastes & preferences.
</p>
                        </div>
                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default Branding