import React, { useEffect, createContext,useReducer,useContext } from "react";
import NavBar from "./components/Navbar";
import { BrowserRouter, Switch, Route, Link,useHistory } from "react-router-dom";
import { reducer, initialState } from './reducers/userReducer'
import "./App.css";
import Work from './components/userScreen/Work'
import UserBlog from './components/userScreen/UserBlog'
import UserProject from './components/userScreen/UserProject'
import Contact from './components/userScreen/Contact'
import AboutUs from './components/userScreen/AboutUs'
import Home from './components/userScreen/Home'
import SignIn from './components/adminScreen/SignIn'
import SignUp from './components/adminScreen/SignUp'
import AdminHome from './components/adminScreen/AdminHome'
// import ProfileProject from './components/adminScreen/ProfileProject'
import AdminBlog from './components/adminScreen/AdminBlog' 
import CreateProject from './components/adminScreen/CreateProject'
import CreateBlog from './components/adminScreen/CreateBlog'
import TypeOfWork from './components/userScreen/TypeofWork'
import Branding from './components/userScreen/typeOfWork/branding'
import UserWD from './components/userScreen/typeOfWork/UserWD'
import UserDM from './components/userScreen/typeOfWork/UserDM'
import Alphabetic from './components/userScreen/Alphabetic'
import ChasmaHome from './components/userScreen/ChasmaHome'
import Creative from './components/userScreen/Creative'
import Career from './components/userScreen/Career'
import Pv from './components/userScreen/typeOfWork/Pv'
import Agromatic from './components/userScreen/ci/Agromatic'
import Upciclo from './components/userScreen/ci/Upciclo'
import Unimech from './components/userScreen/ci/Unimech'
import Scs from './components/userScreen/ci/Scs'
import Kns from './components/userScreen/ci/Kns'
import Roombr from './components/userScreen/ci/Roombr'
import Zourya from './components/userScreen/ci/Zourya'
import Vandy from './components/userScreen/ci/Vandy'
import Animation from './components/userScreen/typeOfWork/Animation'
import HouseOfExperiance from './components/userScreen/typeOfWork/HouseofExperiance'
import SocialMedia from './components/userScreen/typeOfWork/SocialMedia'
import HouseofExperiance from "./components/userScreen/typeOfWork/HouseofExperiance";
import Blog_Authors from './components/userScreen/blog/Blog_Authors';
import Blog_Detail from './components/userScreen/blog/Blog_Detail';
import Blog_Internal from './components/userScreen/blog/Blog_Internal'
import Blog_Internal2 from './components/userScreen/blog/Blog_Internal2'
import Blog_Internal3 from './components/userScreen/blog/Blog_Internal3'
import Blog_Internal4 from './components/userScreen/blog/Blog_Internal4'
import Blog_Internal5 from './components/userScreen/blog/Blog_Internal5'
import Blog_Internal6 from './components/userScreen/blog/Blog_Internal6'

//Creating context
export var UserContext = createContext();

var Routing = () => {

  var history=useHistory()
  var {state,dispatch}=useContext(UserContext)
  useEffect(()=>{
    const user = JSON.parse(localStorage.getItem("user"))
    if(user){
      dispatch({type:"USER",payload:user})
    }else{
      if(!history.location.pathname.startsWith('/user'))
           history.push('/')
    }

    

    

   
  },[])
  return (
    <Switch>

      <Route exact path="/user-home">
        <Home />
      </Route>
      <Route exact path="/user-animation">
        <Animation />
      </Route>
      <Route exact path="/user-he">
        <HouseofExperiance />
      </Route>
      <Route exact path="/user-socialMedia">
        <SocialMedia />
      </Route>
      
      
      <Route exact path="/user-Upciclo">
        <Upciclo></Upciclo>
      </Route>
      <Route exact path="/user-Scs">
        <Scs></Scs>
      </Route>
      <Route exact path="/user-Kns">
        <Kns></Kns>
      </Route>
      <Route exact path="/user-Roombr">
        <Roombr />
      </Route>
      <Route exact path="/user-Zaurya">
        <Zourya />
      </Route>
      <Route exact path="/user-Vandy">
        <Vandy />
      </Route>
      <Route exact path="/">
        <ChasmaHome />
      </Route>
      <Route exact path="/user-pv">
        <Pv />
      </Route>
      <Route exact path="/user-Agromatic">
        <Agromatic />
      </Route>
      <Route exact path="/user-Unimech">
        <Unimech />
      </Route>
      <Route exact path="/user-creative">
        <Creative />
      </Route>
      <Route exact path="/user-work">
        <Work />
      </Route>
      <Route exact path="/user-aboutus">
        <AboutUs />
      </Route>
      <Route exact path="/user-bloguser">
        <Blog_Detail />
      </Route>
      <Route exact path="/user-projectuser">
        <UserProject />
      </Route>
      <Route exact path="/user-contact">
        <Contact />
      </Route>
      <Route exact path="/user-signin">
        <SignIn />
      </Route>
      <Route exact path="/user-signup">
        <SignUp />
      </Route>
      <Route exact path="/admin-home">
        <AdminHome />
      </Route>
      <Route exact path="/profileblog">
        <AdminBlog />
      </Route>
      <Route exact path="/createproject">
        <CreateProject></CreateProject>
      </Route>
      <Route exact path="/createblog">
        <CreateBlog />
      </Route>
      <Route exact path="/user-type-of-work">
        <TypeOfWork />
      </Route>
      <Route exact path="/user-branding">
        <Branding />
      </Route>
      <Route exact path="/user-dm">
        <UserDM />
      </Route>
      <Route exact path="/user-wd">
        <UserWD />
      </Route>
      <Route exact path="/user-alpha">
        <Alphabetic />
      </Route>
      <Route exact path="/user-career">
        <Career />
      </Route>
      <Route exact path="/user-blog_Authors">
        <Blog_Authors />
      </Route>
      <Route exact path="/user-blog_Detail">
        <Blog_Detail />
      </Route>
      <Route exact path="/user-seo">
        <Blog_Internal />
      </Route>
      <Route exact path="/user-cult_branding">
        <Blog_Internal2 />
      </Route>
      <Route exact path="/user-digital_marketing">
        <Blog_Internal3 />
      </Route>
      <Route exact path="/user-moment_marketing">
        <Blog_Internal4 />
      </Route>
      <Route exact path="/user-influncer_marketing">
        <Blog_Internal5 />
      </Route>
      <Route exact path="/user-native_advertising">
        <Blog_Internal6 />
      </Route>
     
      
      
     
    </Switch>
  );
};

function App() {
  var [state,dispatch]=useReducer(reducer,initialState)
 
  return (
    <div>
    
    
   
    <UserContext.Provider value={{state,dispatch}}>
    <BrowserRouter>
      {/* <NavBar /> */}
      <Routing/>
     

    </BrowserRouter>
    </UserContext.Provider>
    </div>
  );
}

export default App;