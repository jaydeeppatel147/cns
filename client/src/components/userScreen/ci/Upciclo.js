import React from 'react'
import NavBar from '../../Navbar'
import Footer from '../UserFooter'


const Upciclo = () => {
    return (
        <div>
            <NavBar></NavBar>
            <div className="container">
                <div className="sectionci">
                    <div className="containerci w-containerci">
                        <div className="div-block-2ci">
                            <h1>Upciclo</h1>
                            <div>Social media strategy & content calendar</div>
                        </div>
                        <div className="div-blockci">
                            <p className="paragraphci">Upciclo is a platform for sellers who currently make or manufacture biodegradable, compostable, upcycled, recycled, pand handmade items for all lifestyle segments.

                            What we do: CNS dug deep for Upciclo & ideated an innovative strategy to help them improve their image and broaden their audience on social media.
</p>
                        </div>
                    </div>
                    <div className="image-containerci w-containerci">
                        <div className="image-holderci _1ci"></div>
                        <div className="image-holderci _2ci"></div>
                        <div className="image-holderci _3ci"></div>
                    </div>
                </div>
                <div className="sectionci">
                    <div className="containerci w-containerci">
                        {/* <div className="div-block-2ci"></div> */}
                        {/* <div className="div-blockci comboci">
                            <p className="paragraphci">&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere&quot;</p>
                            <div className="text-blockci">This is some text inside of a div block.</div>
                        </div> */}
                    </div>
                    <div className="image-containerci w-containerci">
                        <div className="image-holderci _4ci"></div>
                        <div className="image-holderci _5ci"></div>
                        <div className="image-holderci _6ci"></div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default Upciclo