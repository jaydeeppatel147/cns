

import React, { Component, useState, useEffect, useContext } from "react";
import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import UserFooter from './UserFooter'
import Fade from "react-reveal/Fade";
import NavBar from "../Navbar";



const Work = () => {
  var [data, setData] = useState([]);
  var { state, dispatch } = useContext(UserContext);


  useEffect(() => {
    fetch("/allpost2", {
      headers: {
        "Content-Type": "application/json",
        // Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => res.json())
      .then((result) => {
        console.log("Home", result);
        setData(result.posts);
      });
  }, []);



  return (
    <div>
      <NavBar></NavBar>
      <div className="our-work">


        {/* sub heading */}
       
        <div className="work-heading w-container">
          <div className="h1-div">
            <h1 className="h1-work">Work</h1>
          </div>
          <div className="project-div">
            <div className="project">ALL PROJECTS</div>
            <Link to='/user-type-of-work' style={{ textDecoration: 'none' }}> <div class="type-of-work">type of work</div></Link>

          </div>

        </div>

        {/* filter */}
        <div>
          <div className="row radioButton">
            <div className="col" style={{ margin: '0 0 5% 0' }}>
              <Link to="/user-work"> <button type="button" className="btn btn-outline-secondary">Chronological</button></Link>
            </div>
            <div className="col">
              <Link to="/user-alpha">  <button type="button" className="btn btn-outline-secondary">Alphabetical</button></Link>
            </div>

          </div>
        </div>


        {/* img grid */}
        <div className="container" >
          <Fade>
          <div className="row" style={{margin:'10% 0% 0% 0%'}}>
           
            <div className="col">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/agro.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Agromatic</b><br></br>We developed a brochure and a range of vibrant packaging materials, which resonates better with their target audience</p>
              <Link to='/user-Agromatic' ><button type="button" class="btn btn-secondary">Click Here</button></Link>
            </div>
          </div>
          
            </div>
            
            

            <div className="col">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/upciclo.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Upciclo</b><br></br>We strategize upciclo’s social media & develop campaign ideas which help them reach a massive audience</p>
            </div>
            <Link to='/user-Upciclo' ><button type="button" class="btn btn-secondary">Click Here</button></Link>

          </div>
            </div>

            <div className="col">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/unimach.png" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Unimech</b><br></br>CNS designed Unimech’s visiting cards, letter-heads & other in-house tools they use on a daily basis.</p>
            </div>
            <Link to='/user-Unimech' ><button type="button" class="btn btn-secondary">Click Here</button></Link>
          </div>
            </div>
          </div>
          
        

        <div className="row" style={{margin:'3% 0% 0% 0%'}}>
        <div className="col">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/sdc.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Cold storage</b><br></br>We @CNS ideated & created a unique website suited to Cold Storage’s target audience.</p>
            </div>
            <Link to='/user-Scs' ><button type="button" class="btn btn-secondary">Click Here</button></Link>

          </div>
            </div>

            <div className="col">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/kns.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>KNS</b><br></br>We manage KNS’ social media campaign, designed & developed their business website</p>
            </div>
            <Link to='/user-Kns' ><button type="button" class="btn btn-secondary">Click Here</button></Link>
          </div>

            </div>

            <div className="col">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/roomber.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Roombr</b><br />We developed a website for Roombr which is ideal for incorporating the plethora of their unique features</p>
            </div>
            <Link to='/user-Roombr' ><button type="button" class="btn btn-secondary">Click Here</button></Link>
          </div>
            </div>
          </div>


          <div className="row" style={{margin:'3% 0% 0% 0%'}}>
            <div className="col-4">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/zurya.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Zaurya</b><br></br> We enable Zaurya to reach their audience through aggressive, targeted social media campaigns, and ideated/developed a website.</p>
            </div>
            <Link to='/user-Zaurya' ><button type="button" class="btn btn-secondary">Click Here</button></Link>
          </div>
          
            </div>

            


            <div className="col-4">
            <div className="card shadow-lg p-3 mb-5 bg-white rounded" style={{ width: '25rem' }}>
            <img className="card-img-top" src="images/img/vandy.jpg" alt="Card image cap" />
            <div className="card-body">
              <p className="card-text" style={{ fontSize:'12px'}}><b>Vandy's</b><br></br>CNS ideated & produced a video to improve Vandy’s brand positioning in the market.</p>
            </div>
            <Link to='/user-Vandy' ><button type="button" class="btn btn-secondary">Click Here</button></Link>
          </div>
          
            </div>

            
          </div>
          </Fade>
          </div>


        <UserFooter></UserFooter>
      </div>
    </div >
  )
}

export default Work