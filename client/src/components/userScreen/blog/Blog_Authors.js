import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from ;
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Author = () => {
  return (
    <div>
      <NavBar></NavBar>
    <div data-ix="page-loading" className="page-wrapperh1">
      <div className="popup-wrapperh1">
        <div className="contact-window popup-windowh1">
          <h4 className="contact-window-headerh1">Get In Touch</h4>
          <div className="w-formh1">
            <form
              id="email-form"
              name="email-form"
              data-name="Email Form"
              className="contact-window-formh1"
            >
              <label for="name" className="form-labelh1">
                Name:
              </label>
              <input
                type="text"
                id="name"
                name="name"
                data-name="Name"
                placeholder="Enter your name"
                maxlength="256"
                className="input w-inputh1"
              />
              <label for="email-2" className="form-labelh1">
                Email:
              </label>
              <input
                type="email"
                id="email-2"
                name="email-2"
                data-name="Email 2"
                placeholder="Enter your email address"
                maxlength="256"
                required=""
                className="input w-inputh1"
              />
              <label for="field" className="form-labelh1">
                Message:
              </label>
              <textarea
                id="field"
                name="field"
                placeholder="How can we help you?"
                maxlength="5000"
                required=""
                data-name="Field"
                className="input text-area w-inputh1"
              ></textarea>
              <input
                type="submit"
                value="Submit Message"
                data-wait="Please wait..."
                className="button form-button w-buttonh1"
              />
            </form>
            <div className="form-successh1 window-successh1 w-form-doneh1">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div className="form-errorh1 window-errorh1 w-form-failh1">
              <div>Oops! Something went wrong while submitting the form</div>
            </div>
          </div>
        </div>
        <div
          data-w-id="aa67b0c9-ff49-bd15-fe32-4de3defd227c"
          className="popup-overlayh1"
        ></div>
      </div>
      <div className="page-header-sectionh1 blogh1">
        <h1 data-ix="fade-up-1">Blog</h1>
      </div>
      <div className="sectionh1">
        <div className="section-header-wrapperh1">
          <div className="section-small-headerh1">Author</div>
          <h2 className="section-headerh1">Divyansh Grover</h2>
        </div>
        <div className="wrapper w-containerh1">
          <div className="blog-posts-and-sidebarh1">
            <div className="div-block-3h1">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div className="categoryh1 post-card-v1-categoryh1">MARKETING</div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  srcset="images/crew-22256-p-1080.jpeg 1080w, images/crew-22256-p-1600.jpeg 1600w, images/crew-22256.jpg 1620w"
                  src="images/crew-22256.jpg"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4 className="post-card-v1-headerh1">Heading</h4>
              <p className="post-card-v1-texth1">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse varius enim in eros elementum tristique. Duis
                cursus, mi quis viverra ornare, eros dolor interdum nulla, ut
                commodo diam libero vitae erat. Aenean faucibus nibh et justo
                cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus
                tristique posuere.
              </p>
              <div className="post-card-v1-bottomh1">
                <a href="#" className="post-card-v1-authorh1 w-inline-blockh1">
                  <img
                    src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                    alt=""
                    className="post-card-v1-avatarh1"
                  />
                  <div>Divyansh</div>
                </a>
                <div className="blog-card-v1-doth1"></div>
                <div className="text-block-5h1">Published on 5th Dec</div>
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrowh1 w-inline-blockh1"
                >
                  <div className="link-arrow-texth1">Read More</div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>
            <div
              data-ix="fade-left"
              data-w-id="9c529e03-2777-7093-56ac-bd21ac14a289"
              style={{opacity:0}}
              className="blog-sidebarh1"
            >
              <div className="blog-sidebar-blockh1">
                <h5 className="sidebar-headerh1">Categories</h5>
                <a href="#" className="sidebar-categoryh1 w-inline-blockh1">
                  <div className="blog-category-doth1"></div>
                  <div>Design</div>
                </a>
              </div>
              <div className="blog-sidebar-blockh1">
                <h5 className="sidebar-headerh1">Tags</h5>
                <a href="#" className="sidebar-tagh1">
                  Text Link
                </a>
              </div>
              <div className="blog-sidebar-blockh1">
                <h5 className="sidebar-headerh1">Other Authors</h5>
                <a href="#" className="sidebar-authorh1 w-inline-blockh1">
                  <img
                    src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                    alt=""
                    className="sidebar-author-avatarh1"
                  />
                  <div>This is some text inside of a div block.</div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Author;
