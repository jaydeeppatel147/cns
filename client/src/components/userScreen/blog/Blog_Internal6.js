import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Internal = () => {
  return (
    <div>
    <NavBar></NavBar><br></br><br></br>
    <div data-ix="page-loading" className="page-wrappersidebar-headerh1">
      <div className="post-header-sectionh1">
        <div className="wrapperh1 w-containerh1">
          <div className="post-header-wrapper">
            <a href="#" data-ix="fade-up-1" className="categoryh1">
              {/* DESIGN */}
            </a>
            <h1 data-ix="fade-up-2">
                
              {/* When you are down and out, how to <br /> */}
              {/* keep going */}
            </h1>
            <a
              href="#"
              data-ix="fade-up-3"
              className="post-header-authorh1 w-inline-blockh1"
            >
              <div>Post by</div>
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-header-avatarh1"
              />
              <div>Divyansh</div>
            </a>
          </div>
        </div>
        <div className="post-header-overlayh1"></div>
      </div>
      <div className="sectionh1">
        <div className="wrapper w-containerh1">
          <div className="blog-post-contenth1">
            <div className="blog-post w-richtexth1">
              <h2><b>
              Marketing in camouflage: Native Advertising</b></h2>
              <p>
              No one wants to see an advertisement in the middle of something, so now, it’s a challenging task for advertising agencies to capture the users’ attention. Now, what they are doing is creating content which will not only advertise their brand, but will also provide sincere and interesting content. Native advertising is a paid form of advertisement which is inherently non-disruptive. It is slighted as an actual, reliable content that actually fits in. Native advertisements create interests and not interruptions, which mean people find these relevant and interesting. The dragon challenge by Range Rover where a FORMULA 1 driver drives up to Heaven’s Gate is a terrific example of native advertising. Such feats not only grab people’s attention, but they also subtly advertise the brand’s product.

              </p>
              <br />
              <h3>
              How can you identify a native ad?

              </h3>

              <p>
              Although Native advertising is camouflage in nature, you can still identify them by certain format issued by Interactive Advertising Bureau (IAB)

              </p>
              <br/>


              <ol>
                  <li>
                  In-feed ads- These are the ads that appear directly in line with other articles or posts on various social media platforms or on a publisher’s website. They differ on various sites as they are made to fit in.

                  </li>
              </ol>

              <ol>
                  <li>
                  Search and promoted listings- The articles which appear on the top when you search something, below which the ad is written are the native ads that the company bids on. They are made to look like organic search results. Several promotional listings also appear on online sites which are paid for, but actually look organic.
                  </li>
              </ol>
              <p>
              Content recommendation- These ads are either on the side of a web page or just below an article which is recommended by the site based on the content you have just viewed.

              </p>
              <br/>
              <p>
              Suggested or sponsored posts- These ads appear on any social media platform, and the text always shows either ‘sponsored’ or ‘suggested’ written on it.

              </p>

              <p>
              Search and promoted listings- The articles which appear on the top when you search something, below which the ad is written are the native ads that the company bids on. They are made to look like organic search results. Several promotional listings also appear on online sites which are paid for, but actually look organic.
              </p>
              <br/>
              <h1>
              Benefits of native advertising

              </h1>
              <ol>
                  <h4>
                  Future of Native Advertising

                  </h4>
                <li>
                In comparison to other ads which pop up on the screen and interrupt a user’s search, native advertisements are able to attract more customers by blending in.

                </li><br/>
                    <li>
                    As it uses the camouflage technique and creates content which fits in, users do not even mind using it, as it turns out to be useful and informative.
                    </li>

                <li>How to do things </li>
                <br/>
                <li>Setting reminders</li>
                <li>Entertainment purposes, and</li>
                <li>It’s a win-win opportunity for all the parties. The user enjoys the content with no intrusion, the publisher does not have to worry about losing credibility, and the brand can effectively communicate without worrying about annoying its viewers.
</li>
              </ol>
              <p>
                Why people have started using voice search is a question worth
                answering to. In this fast-paced world, we don’t have patience
                and we want things quick & easy. Voice search helps you to do
                that. We do so many things at once, that we all need an
                assistant. But why hire one, when the smartphone you have can be
                your assistant. Yes, ask your Google assistant to do things for
                you, and you can shift your focus to more important things.
              </p>
              <br />
              <p>
                If we consider older people, they are more comfortable talking,
                than typing. Voice search is increasing its efficiency by
                including different accents and words to make it more convenient
                to use.
              </p>
              <h4>Benefits of voice search optimization</h4>

              <ul>
                <li>
                  It is conversational, and thus adds a little bit of human
                  touch, making people use it more.
                </li>
                <li>
                  It is easy to access and gives you the most appropriate
                  result. Generally, the result it shows is exactly what people
                  are looking for. So, they don’t have to scroll through many
                  sites.
                </li>
                <li>It is convenient to use.</li>
                <li>
                  Also, people like to do random searches just for entertainment
                  and pass their time.
                </li>
                <li>
                  When you don’t know what to type, you can easily say it out
                  and the results will be there.
                </li>
              </ul>
              <p>
                <b>Voice search optimization for website</b>
              </p>
              <p>
                A research predicted that voice search will account for half the
                searches by the year 2020. With such expeditious increase, it is
                going to affect the course of digital marketing. Businesses have
                to keep up with the pace and will have to include voice search
                optimisation into their digital strategies. To do that, they
                will have to optimize their sites accordingly.
              </p>
              <h3>SEO optimization for voice search-</h3>
              <br />
              <ol type="1">
                <li>
                  Long-tail keywords- There are differences in the use of
                  keywords when you search by typing and by speaking. Example,
                  you will normally type ‘weather today’ but if you voice
                  search, you’ll ask ‘what is the weather today’. When you do a
                  voice search, you tend to ask questions or speak longer
                  sentences, which means a brand needs to include long tail
                  keywords making it more conversational and relevant.
                </li>
                <br />
                <li>
                  Featured snippets- There is a direct relation between feature
                  snippets and voice search. Many voice search results come in
                  the form of feature snippets. People generally ask questions
                  when they search verbally, so it is beneficial to keep that in
                  mind and give results accordingly.
                </li>
                <br />
                <li>
                  Website speed- The faster your site appears the more are the
                  chances of it being presented to users. A fast loading page
                  will also have an impact on WordPress search ratings.
                </li>
                <br />
                <li>
                  Position zero optimization- When you type your query there are
                  many articles to choose from, but when you do a voice search
                  there is only one result which is the zero position. So, if
                  the brand wants to be the zero position, they have to
                  understand what questions users will most often ask, and
                  create the content with the keywords users will most often
                  use.
                </li>
                <br />
                <li>
                  Local SEO- More than 22% of the users use voice search to find
                  local information. People generally search “things to do near
                  me,” and including these in your content will result in more
                  visits to your site. Include keywords that local people use to
                  describe the things around them.
                </li>
                <br />
                <li>
                  Mobile optimization- We can say that voice search is
                  exclusively mobile. Although there is Cortana on desktops,
                  people mostly use voice search on their phones. When 60% of
                  the searches are done on mobile phones, it is essential to
                  build the website accordingly and be ahead of the competitors.
                </li>
                <br />
              </ol>
              <h1>
                <b>Conclusion</b>
                </h1><br/>
                <p>
                  The busier our lives get, the more we’ll look for faster and
                  easier solutions. As technology evolves and voice assistants
                  become smarter and more accurate, consumers will use them to
                  find information to save time, and why shouldn’t they?
                  Technology has always given us relief from tedious things to
                  do, making it easier for us at every step.
                </p><br/>
                <p>
                  There is so much potential in voice search optimization. With
                  users always on the go, voice search is only going to continue
                  to dominate the mobile environment. Not only is it easy to
                  use, it also saves a lot of time by providing the most
                  accurate result in the very first time. Many tech giants like
                  Google and Apple have already optimized for voice search to
                  have an edge over the others. Specializing in voice search SEO
                  and monetizing it, is critical to compete. Therefore,
                  marketers need to take advantage of this soon to be the
                  popular trend and make the most out of it.
                </p>
              
            </div>
            <div className="post-date-wrapperh1">
              <div>Published</div>
              <div className="post-dateh1">on 5th Dec</div>
            </div>
            <div className="post-author-profileh1">
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-author-avatarh1"
              />
              <div className="post-author-infoh1">
                <h5 className="post-author-nameh1">Divyansh</h5>
                <div className="post-author-bioh1">
                  This is some text inside of a div block.
                </div>
                <a href="#" className="post-author-linkh1">
                  View All Posts
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="section grey-sectionh1">
        <div className="section-header-wrapperh1">
          <div className="section-small-headerh1">More From Blog</div>
          <h2 className="section-headerh1">You Might Also Like</h2>
        </div>
        <div className="wrapper w-containerh1">
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
        </div>
      </div> */}
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Internal;
