import React from 'react'

import NavBar from "../../Navbar";
import UserFooter from '../UserFooter'

const Branding = () => {
    return (
        <div>
            <NavBar></NavBar><br></br>
            <div className="branding-2">

                {/* main img */}
                {/* <div className="branding-img">
                    <img src="images/branding1.jpg" loading="lazy" sizes="(max-width: 1920px) 100vw, 1920px"  alt="" /><div className="center">srhsrh</div>
                </div> */}

                <div class="containerImg">
                    <img src="images/branding1.jpg" alt="Snow" sizes="(max-width: 1920px) 100vw, 1920px" />

                    <div class="centeredImg">

                        <div className="branding2">
                            <br></br><br></br>
                            <b className="textImg">BRANDING</b>
                        </div>


                    </div>
                </div>

                <div className="branding-container w-container">
                    <div className="w-layout-grid grid-12">
                        <div>
                            <h1 className="heading-24">What we do</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">Services</h2>
                            <p className="paragraph-5">We provide a complete 360-degree Brand Identity strategy. At CNS we go the extra-mile and also brief you about the ideal way to take your brand to market </p>
                            <h2 className="heading-25">Roles &amp; responsibility</h2>
                            <p className="paragraph-5">We at CNS give birth to your brand. From picking the perfect Brand name, tagline, logo to creating them to your liking. We take care of the ideating & designing process spearheaded by our talented team of marketing experts, designers & content writers
</p>
                        </div>
                        <div>
                            <h1 className="heading-24">Process</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">How we do things</h2>
                            <p className="paragraph-5">We do our research and find out what has been successful for similar brands in the past. Post this, we at CNS come up with unique & out-of-the box solutions/strategies to ensure that your brand is in the spotlight</p>
                        </div>
                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default Branding