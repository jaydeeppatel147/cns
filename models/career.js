const mongoose = require("mongoose");
const { Schema } = mongoose;
const careerSchema = new Schema({
    name:{type:String},
    number:{type:Number},
    email:{type:String},
    like_to_do:{type:String},
    upload_CV:{type:String},
    portfolio:{type:String},
    message:{type:String}
}, {timestamps : true})
const career = mongoose.model("career", careerSchema);
module.exports = career;