import React from 'react'
import NavBar from '../../Navbar'
import Footer from '../UserFooter'


const Unimech = () => {
    return (
        <div>
            <NavBar></NavBar>
            <div className="container">
                <div className="">
                    <div className="containerci w-containerci">
                        <div className="div-block-2ci">
                            <h1>Unimech</h1>
                            <div>Branding & in-house communication tools design</div>
                        </div><br></br><br></br>
                        <div className="div-blockci">
                            <p className="paragraphci">Leaders in delivering high quality complex tools, mainly for the requirements of aerospace, Oil & Gas industries
                            <br></br>
                            What we do: We re-designed many of the communication tools Unimech uses on a daily basis. Created a style guide, redesigned their logo, invoice, letter-head & visiting card.



</p>
                        </div>
                    </div>

                    <div className="rowj">
                    <div className="columnj">
                        <img src="images/unimech/1.png" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/unimech/2.png" className="m-3 p-3" style={{width:'100%'}} />
                        

                    </div>
                    <div className="columnj">
                        <img src="images/unimech/3.png" className="m-3 p-3" style={{width:'100%'}} />
                        {/* <img src="images/agromatic/4.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                        

                    </div>
                    <div className="columnj">
                        <img src="images/unimech/4.png" className="m-3 p-3" style={{width:'100%'}} />
                        {/* <img src="images/agromatic/6.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                       

                    </div>
                    <div className="columnj">
                        <img src="images/unimech/5.png" className="m-3 p-3" style={{width:'100%'}} />
                        {/* <img src="images/agromatic/8.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                        

                    </div>
                    
                </div>

                </div>
                
            </div>
            <div style={{marginTop: '0%'}}>
            <Footer></Footer>
            </div>
            
        </div>
    )
}

export default Unimech