import React, { Component, useState, useContext } from "react";
import M from "materialize-css";

const MiniContactForm = () => {
  var [name, setName] = useState("");
  var [number, setNumber] = useState();
  var [email, setEmail] = useState("");
  var [txt, setTxt] = useState("");
  console.log(name,number)


  var PostData = () => {
    fetch("/contact_Us", {
      method: "post",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name,
        email,
        number,
        txt
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data.user);
        if (data.error) {
          M.toast({ html: data.error, classes: "#D50000 red accent-4" });
        } else {
          // localStorage.setItem("jwt",data.token);
          // localStorage.setItem("user", JSON.stringify(data.user));
          // dispatch({type:"USER",payload:data.user})
          M.toast({
            html: " Data Posted successfully",
            classes: "#4CAF50 green"
          });
          //history.push("/");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  return (
    <div>
      <div className="contact">
        <div className="container-5 w-container">
          <div className="contact-heading">
            <h2 className="heading-5">We’re a bunch of cool guys</h2>
            <div className="text-block-4" style={{ color: "white" }}>
              Give us your details and let’s do something unique and viral for
              your brand. We’re always up for a coffee anyways.
            </div>
            {/* form */}
            <br></br>
            {/* <form> */}
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Name*"
                  onChange={e => setName(e.target.value)}
                  value={name}
                />
              </div>
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Phone Number*"
                  onChange={e => setNumber(e.target.value)}
                  value={number}
                />
              </div>
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Email*"
                  onChange={e => setEmail(e.target.value)}
                  value={email}
                />
              </div>
              <div class="form-group">
                <textarea
                  class="form-control"
                  style={{ height: "100px" }}
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  rows="3"
                  placeholder="  What do you want to do with us?"
                  onChange={e => setTxt(e.target.value)}
                  value={txt}
                ></textarea>
              </div>
              <br></br>
              <center>
                <button
                  type="submit"
                  className="btn btn-dark btn-lg"
                  onClick={() => PostData()}
                >
                  Submit
                </button>
              </center>
            {/* </form> */}
          </div>
        </div>
      </div>
    </div>
  );
};
export default MiniContactForm;









