import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Internal = () => {
  return (
    <div>
    <NavBar></NavBar><br></br><br></br>
    <div data-ix="page-loading" className="page-wrappersidebar-headerh1">
      <div className="post-header-sectionh1">
        <div className="wrapperh1 w-containerh1">
          <div className="post-header-wrapper">
            <a href="#" data-ix="fade-up-1" className="categoryh1">
              {/* DESIGN */}
            </a>
            <h1 data-ix="fade-up-2">
              {/* When you are down and out, how to <br />
              keep going */}
            </h1>
            <a
              href="#"
              data-ix="fade-up-3"
              className="post-header-authorh1 w-inline-blockh1"
            >
              <div>Post by</div>
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-header-avatarh1"
              />
              <div>Divyansh</div>
            </a>
          </div>
        </div>
        <div className="post-header-overlayh1"></div>
      </div>
      <div className="sectionh1">
        <div className="wrapper w-containerh1">
          <div className="blog-post-contenth1">
            <div className="blog-post w-richtexth1">
              <h2>
                <b>Digital marketing: the new normal</b>
              </h2>
              <br />
              <p>
                Gone are the days when people sent letters to each other. Now,
                everyone can connect with anyone in seconds, via a call or by a
                text. There are more mobile phones than there are people in this
                world. The usage of phones has increased so much that people use
                their phones even in washrooms. It is very important to keep
                track of what is happening and strategize the marketing
                techniques accordingly. With it, the new wave has hit the shore,
                and it is digital media marketing. There’s probably no brand
                whose presence is not there on any social media platform. Any
                brand which uses internet and online based digital technology
                like laptop, smartphone or any other digital media platforms to
                promote their product and services and reach their customers is
                using digital marketing.
              </p>
              <br />
              <p>
                One of the social media marketing techniques that a brand uses
                is Meme Marketing. Almost every day we see so many memes but
                very few capture our attention that we forward or share with our
                friends on social sites. Memes began as an inside joke, but it
                has now become a medium through which firms communicate. With
                social media continually on the rise, meme marketing has been
                adopted by hundreds of brands as their marketing strategies.
                Memes are easy to make, and they can go viral in minutes. What
                more would a company want than seeing people sharing their posts
                and talking about them. One of the best examples of all time
                would be Netflix. This brand does not leave any single
                opportunity to link its product with the current on-going trend.
              </p>
              <br />
              <p>
                There are many reasons why a brand will choose digital marketing
                over any other marketing. Some of them are-
              </p>
              <ol>
                <li>
                  Real time engagement- Marketing has always been about
                  connecting with your audience, in the right place at the right
                  time. With the internet, it is possible to keep track of what
                  people desire by keeping a tab on their insights and
                  activities. You can get in touch with them with just a click.
                </li>
                <li>
                  Cheap cost- When compared to big billboards and traditional
                  methods, social media marketing is a whole lot cheaper. You
                  can create a normal digital base free of cost, and if you want
                  a dynamic and creative one, it is still cheaper than rest.{" "}
                </li>
                <li>
                  High exposure- The digital world is not confined to
                  geographical boundaries, so when you choose digital marketing,
                  you are making your{" "}
                </li>
                <li>
                  Viral- Meme marketing, as we talked about earlier, gives a
                  picture of how the content can go viral in seconds. Every
                  brand wants people to talk about them, remember them, and they
                  do so by keeping up with what is trending. They advertise or
                  make a meme that people will share, which increases their
                  traffic in return.
                </li>
                <li>
                  It’s a two way street- Customers also make the best use of
                  digital marketing by giving feedback or reviews about the
                  products. It also helps them get knowledge about any brand
                  within minutes by fully researching it on social media
                  platforms.
                </li>
                <li>
                  Remembrance- Science has proven that a person remembers more
                  things when they see something, rather than reading about it.
                  And when there is no space constraint you can add as many
                  visuals you want for your brand. It will be a two shot by one
                  target, first it will grab the attention quickly and second
                  the person will retain it for a long period of time.
                </li>
              </ol>
              <br />
              <p>
                The recent covid-19 situation has proved that digital marketing
                is ever-lasting. Even when traditional ways were unavailable
                during lockdown, digital ways were still open. People could buy
                things online without getting in touch with anyone. Also, a
                brand needs to be updated as the others and understand how you
                can leverage the latest digital marketing techniques to grow
                your reach. You can also create your brand reputation by using
                various social media platforms, by running different campaigns,
                raising awareness, acknowledging that you are aware about what
                is happening in the world, and then building your customers’
                trust in the process.
              </p>
              <br />
              <p>
                The share of people spending more time on electronic devices is
                only going to increase in the future. Gone will be the days of
                billboard and traditional marketing, as everyone will be busy
                looking at their phones or pads. Either they’ll be busy
                texting/calling, or will be watching something on the internet.
                Not a single person will look out at the road and even if they
                do, it won’t beto check out advertisements. Even the driver
                won’t, as Elon Musk has promised to deliver self-driven cars in
                the near future, and with people spending so much of their time
                on the internet, it is logical to advertise your brand on that.
              </p>
             
            </div>
            <div className="post-date-wrapperh1">
              <div>Published</div>
              <div className="post-dateh1">on 5th Dec</div>
            </div>
            <div className="post-author-profileh1">
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-author-avatarh1"
              />
              <div className="post-author-infoh1">
                <h5 className="post-author-nameh1">Divyansh</h5>
                <div className="post-author-bioh1">
                  This is some text inside of a div block.
                </div>
                <a href="#" className="post-author-linkh1">
                  View All Posts
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="section grey-sectionh1">
        <div className="section-header-wrapperh1">
          <div className="section-small-headerh1">More From Blog</div>
          <h2 className="section-headerh1">You Might Also Like</h2>
        </div>
        <div className="wrapper w-containerh1">
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
        </div>
      </div> */}
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Internal;
