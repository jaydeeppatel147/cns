import React, { Component, useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import M from "materialize-css";


const Creative = () => {

    var [name, setName] = useState("");
    var [number, setNumber] = useState();
    var [email, setEmail] = useState("");
    var [txt, setTxt] = useState("");
    console.log(name, number)


    var PostData = () => {
        fetch("/contact_Us", {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                name,
                email,
                number,
                txt
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data.user);
                if (data.error) {
                    M.toast({ html: data.error, classes: "#D50000 red accent-4" });
                } else {
                    // localStorage.setItem("jwt",data.token);
                    // localStorage.setItem("user", JSON.stringify(data.user));
                    // dispatch({type:"USER",payload:data.user})
                    M.toast({
                        html: " Data Posted successfully",
                        classes: "#4CAF50 green"
                    });
                    //history.push("/");
                }
            })
            .catch(error => {
                console.log(error);
            });
    };







    return (
        <div className="">
            <div className="expeimental">

                {/* logo */}
                <div className="exp-container w-container">
                    <h1 className="exp-heading"><span className="text-span-3">where to</span></h1>
                    <h1 className="exp-heading-2"><span className="span-2">find us</span></h1>
                </div>

                {/* address */}
                <div className="exp-div">
                    <h1 className="exp-heading-3">ONE CO.WORK, 80 FT ROAD KORAMANGALA, BENGALURU - 560034</h1><img src="images/Group-29.png" loading="lazy" sizes="(max-width: 479px) 85vw, (max-width: 767px) 50vw, (max-width: 991px) 300px, 37vw" srcset="images/Group-29-p-500.png 500w, images/Group-29.png 609w" alt="" className="exp-img-1" />
                </div>

                {/* Mobile email */}
                <div className="exp-container-2 w-container">
                    <div className="w-layout-grid grid-10">
                        <div className="grid-10-div">
                            <a href="#" className="contact-number">+91 7387027926</a>
                            <a href="#" className="contact-number">+91 7073155192</a>
                        </div>
                        <div className="grid-10-div-2">
                            <a href="#" className="contact-number">sales@chalksnslate.com</a>
                        </div>
                        <div class="grid-10-div">
                            <a href="#" className="contact-number">+91 9654893769</a>
                            <a href="#" className="contact-number"></a>
                        </div>
                    </div>
                </div>

                {/* form  */}
                <div className="exp-container-3 w-container">
                    <div className="w-layout-grid grid-11">
                        <div id="w-node-ebeddb265d4b-a53ffc97" className="exp-column-div">
                            <h1 className="exp-heading-4">We’re a bunch of cool guys.</h1>
                            <div className="exp-para">Give us your details and let’s do something unique and viral for your brand.</div>
                        </div>
                        <div id="w-node-ebeddb265d50-a53ffc97" className="exp-column-div-2">
                            <div className="experimental-form w-form">
                                {/* <form id="email-form" name="email-form" data-name="Email Form">
                <input type="text" className="text-field w-input" maxlength="256" name="name-3" data-name="Name 3" placeholder="Name*" id="name-3" />
                <input type="tel" className="text-field-2 w-input" maxlength="256" name="Phone-3" data-name="Phone 3" placeholder="Phone Number*" id="Phone-3" required="" />
                <input type="text" className="text-field w-input" maxlength="256" name="Email-4" data-name="Email 4" placeholder="Email*" id="Email-4" required="" />
                <textarea placeholder="What do you want to do with us?" maxlength="5000" id="Message-3" name="Message-3" data-name="Message 3" className="textarea text-field w-input"></textarea>
                <input type="submit" value="Submit" data-wait="Please wait..." className="submit-button textarea exp-form-button w-button">

                </form>
             */}

<div className="contacts">
        <div className="containser-5 w-cosntainer">
          <div className="contact-headin">
            {/* <h2 className="heading-5">We’re a bunch of cool guys</h2>
            <div className="text-block-4" style={{ color: "white" }}>
              Give us your details and let’s do something unique and viral for
              your brand. We’re always up for a coffee anyways.
            </div> */}
            {/* form */}
            <br></br>
            {/* <form> */}
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Name*"
                  onChange={e => setName(e.target.value)}
                  value={name}
                />
              </div>
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Phone Number*"
                  onChange={e => setNumber(e.target.value)}
                  value={number}
                />
              </div>
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Email*"
                  onChange={e => setEmail(e.target.value)}
                  value={email}
                />
              </div>
              <div class="form-group bg-white">
                <input
                 type="text"
                 style={{ height: "50px" }}
                 className="form-control"
                 id="exampleInputEmail1"
                 aria-describedby="emailHelp"
                  placeholder="Message*"
                  onChange={e => setTxt(e.target.value)}
                  value={txt}
                ></input>
              </div>
              <br></br>
              <center>
                <button
                  type="submit"
                  className="btn btn-dark btn-lg"
                  onClick={() => PostData()}
                >
                  Submit
                </button>
              </center>
            {/* </form> */}
          </div>
        </div>
      </div>
    























                            </div>
                        </div>
                    </div>
                </div>

                {/* navbar */}

                <div className="exp-nav-div" id='trial' >
                    <div className="exp-nav w-container"  >
                        <Link to="/user-aboutus" style={{ textDecoration: 'none' }}><h2 className="exp-nav-heading nav-first">About us</h2></Link>
                        {/* <h2 className="exp-nav-heading">Heading</h2> */}
                        <Link to="/user-home" style={{ textDecoration: 'none' }}><h1 className="exp-nav-main-heading">CHALKS &amp; SLATE</h1></Link>
                        {/* <h2 className="exp-nav-heading">Community</h2> */}
                        <Link to="/user-contact" id='what' style={{ textDecoration: 'none' }}><h2 className="exp-nav-heading nav-last">contact us</h2> </Link>
                    </div>
                </div>

                {/* block */}
                <div className="block" >
                    <div className="box-block" style={{ margin: '1% 5% 0 0' }}>
                        <p className="heading-18" style={{ margin: '3% 0 0 0%' }}>Out-of-the-box solutions</p><img src="images/box-1.png" loading="lazy" sizes="(max-width: 767px) 70vw, (max-width: 991px) 65vw, 50vw" srcset="images/box-1-p-500.png 500w, images/box-1.png 787w" alt="" className="image-5" />
                    </div>
                    <div className="box-block box-block-2" style={{ margin: '1% 5% 0 0' }}>
                        <p className="heading-18 heading-19" style={{ margin: '0% 0 0 5%' }}>Viral Campaigns</p><img src="images/box-2.png" loading="lazy" sizes="(max-width: 991px) 52vw, 50vw" srcset="images/box-2-p-500.png 500w, images/box-2.png 787w" alt="" className="image-5 image5-2" />
                    </div>
                    <div className="box-block box-block-3" style={{ margin: '1% 50% 0 0' }}>
                        <p className="heading-18 heading-20" style={{ margin: '3% 0 0 12%' }} >Unique Ideas</p><img src="images/box-3.png" loading="lazy" sizes="(max-width: 767px) 100vw, 500px" srcset="images/box-3-p-500.png 500w, images/box-3.png 689w" alt="" className="image-5 imag3-7" />
                    </div>
                </div>

                {/* hello header */}
                <div className="exp-div-mid">
                    <img src="images/Image-1.png" loading="lazy" alt="" className="arrow" />
                    <img src="images/Path-4.png" loading="lazy" alt="" className="pointer" />
                    <img src="images/d444183b-5735-4441-89e6-baeee4c3c610-removebg-preview.png" loading="lazy" alt="" className="man" />
                    <img src="images/Group-15.png" loading="lazy" alt="" className="dots-2" />
                </div>

                <div className="exp-container-7 w-container">
                    <h3 className="heading-21">so who are we?</h3>
                    <h1 className="heading-22">the fun guys</h1>
                    <div className="who-div">
                        <h1 className="heading-23">who transform your</h1>
                        <h1 className="heading-23 _1">who transform your</h1>
                        <h1 className="heading-23 _2">who transform your</h1>
                        <h1 className="heading-23 _3">who transform your</h1>
                    </div>
                    <h1 className="heading-22">BRAND, WEBSITE INTO SOMETHING</h1>
                    <div className="who-div">
                        <h1 className="heading-23 _24">meaningful</h1>
                        <h1 className="heading-23 _1 _24-1">meaningful</h1>
                        <h1 className="heading-23 _2 _24-2">Meaningful</h1>
                        <h1 className="heading-23 _3 _24-3">Meaningful</h1>
                    </div>
                </div>


            </div>

            {/* section 2 */}
            {/* <div className="experimental-2">
                <center>
                <div className="exp-div-3">
                    <div className="text-block-11">WE’RE THE EXPERIMENTAL GUYS</div><br></br>
                   
                </div>
                </center>
                <div className="exp-div-4">
                    <div className="exp-div-block">
                        <img src="images/dotbg2.png" loading="lazy" alt="" className="image-4" />
                        <p className="paragraph-4">We have the guts to go beyond conventions, and execute some of the most innovative ideas you will ever see. In this dynamic era digital marketing, we create, solve, and brainstorm solutions to every problem your brand may encounter on its journey to greatness. 
                       <br></br><br></br>

                       ChalksnSlate, with its experienced team of talented designers, writers and developers, is revolutionizing how we use the digital space, in terms of brand development, marketing and optimizing the way different brands strategize for social & digital media.
                        </p>
                        

                    </div>
                    <div className="bean-img">
                        <h3 className="bean-text">LOREM IPSUM DOLOR SIT AMET</h3>
                        <img src="images/g1.png" loading="lazy" alt="" className="bean" />
                    </div>
                </div>
                <div className="exp-div-5">
                    <br></br><br></br>
                    <img src="images/g1.png" loading="lazy" alt="" className="frame-4" />
                    <img src="images/g2.png" loading="lazy" alt="" className="frame-3" />
                    <img src="images/g3.png" loading="lazy" alt="" className="frame-1 size" />
                    <img src="images/g4.png" loading="lazy" alt="" className="frame-2" />
                    <img src="images/g5.png" loading="lazy" alt="" className="frame-5" />
                    <img src="images/g6.png" loading="lazy" alt="" class="frame-6" />
                    <div className="row">
                        <div className="col">
                        <img src="images/dotbg.png" loading="lazy" alt="" className="dots" />
                        </div>
                        <div className="col">
                         <img src="images/dotbg.png" loading="lazy" alt="" className="dots" />
                        </div>

                    </div>
                   
                   
                </div>
            </div> */}


            <div className="experimental-2-cns">
                <div className="exp-div-3-cns">
                    {/* <div className="text-block-cns">WE’RE THE EXPERIMENTAL GUYS</div> */}
                    {/* <h1 className="exp-heading-cns">LOREM IPSUM DOLOR SIT</h1>
                    <h1 className="exp-heading-6-cns">LOREM IPSUM DOLOR</h1> */}
                </div>
                <div className="exp-div-4-cns">
                    <div className="exp-div-block-cns">
                        <img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe32f722cb0f42a1c8c4614_Mask%20Group%201.png" loading="lazy" alt="" className="image-4-cns" />
                        <p className="paragraph-4-cns">We have the guts to go beyond conventions, and execute some of the most innovative ideas you will ever see. In this dynamic era digital marketing, we create, solve, and brainstorm solutions to every problem your brand may encounter on its journey to greatness.
                        <br></br><br></br>

ChalksnSlate, with its experienced team of talented designers, writers and developers, is revolutionizing how we use the digital space, in terms of brand development, marketing and optimizing the way different brands strategize for social & digital media.</p>
                    </div>
                    <div className="bean-img-cns">
                        <h3 className="bean-text-cns">WE’RE THE EXPERIMENTAL GUYS</h3><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe32f72112dc4bdca75e5c1_Group%20225.png" loading="lazy" alt="" className="bean-cns" />
                    </div>
                </div>
                <div className="exp-div-5-cns"><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe3357bc331aaef6637322b_Group%20226.png" loading="lazy" alt="" className="frame-4-cns" /><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe3357b0dd8345c3b713e45_Group%20229.png" loading="lazy" alt="" className="frame-3-cns" /><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe3357be64c87179384208f_Group%20228.png" loading="lazy" alt="" className="frame-1-cns" /><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe3357b052e453539abbda6_Group%20225.png" loading="lazy" alt="" className="frame-2-cns" /><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe3357b23c64c04a5870ff6_Group%20230.png" loading="lazy" alt="" className="frame-5-cns" /><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe3357b35f22119de7bac6b_Group%20227.png" loading="lazy" alt="" className="frame-6-cns" /><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fe333cad047853955c7d06d_Repeat%20Grid%202.png" loading="lazy" alt="" className="dots-cns" /></div>
            </div>

            {/* final part */}

            <div className="experimental-3">
                <div className="testimonials experimental-testimonial w-container">
                <section id="testim" className="testim" style={{margin:'310% 0 20% 0'}}>
                <div className="testim-cover">
                    <div className="wrap">
        
                        <span id="right-arrow" className="arrow right fa fa-chevrxon-right"></span>
                        <span id="left-arrow" className="arrow left fa fa-chexvron-left "></span>
                        <ul id="testim-dots" className="dots">
                            <li className="dot active"></li>
                            
                            <li className="dot"></li>
                            <li className="dot"></li>
                        </ul>
                        <div id="testim-content" className="cont">
                            
                            <div className="active">
                                {/* <!-- <div class="img"><img src="https://image.ibb.co/hgy1M7/5a6f718346a28820008b4611_750_562.jpg" alt=""></div> --> */}
                                <div className="row">
                                    <div className="col">
                                        {/* <div className="img"><img src="images/1.jpg" alt="" /></div> */}
                                    </div>
                                    <div className="col">
                                        <div className="img"><img src="images/2.jpeg" alt="" /></div>
                                    </div>
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t33.png" alt="" /></div> */}
                                    </div>
                                    

                                </div>
                                <h2>Vishal</h2>
                                <p style={{color: 'black'}}>We at Agromatic are highly impressed with the creativity that CNS has brought out in re-designing our in-house communication tools packaging. CNS exceeded our expectation, in terms of both quality of work as well as promptness of deliverables</p>                    
                            </div>
        
                            <div>
                                <div className="row">
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t2.jpeg" alt="" /></div> */}
                                    </div>
                                    <div className="col">
                                        <div className="img"><img src="images/t33.png" alt="" /></div>
                                    </div>
                                    <div className="col">
                                        {/* <div className="img"><img src="images/1.jpg" alt="" /></div> */}
                                    </div>
                                    

                                </div>
                                <h2>Abhishek</h2>
                                <p style={{color:'black'}}>Chalksnslate is a company that walks the talk. They do believe that relationships are key to the business… It is one of our closest partners and is one that is really really looking at ways to improve the product and relationship.</p>                    
                            </div>

                            <div>
                                <div className="row">
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t33.png" alt="" /></div> */}
                                    </div>
                                    <div className="col">
                                        <div className="img"><img src="images/1.jpg" alt="" /></div>
                                    </div>
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t2.jpeg" alt="" /></div> */}
                                    </div>
                                    

                                </div>
                                <h2>Vendy's</h2>
                                <p style={{color:'black'}}>I was wishing to add some pretty pictures ,and a video,  onto my website, and for that, I entrusted the job to Justin.
Justin, who assured me the work will be done well.
 I was not disappointed , and in the end , had some good theme based pictures, and a good video as well.
Will like to recommend ChalksnSlate Media for such  a requirement, in future.</p>                    
                            </div>
        
                           
        
                            
        
                            
        
                        </div>
        
                    </div>
                </div>
            </section>
                </div>
                {/* <div className="exp-container-5 w-container">
                    <h1 className="exp-heading-6">Vishrut, KNS Group</h1>
                    <div className="text-block-12">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud”</div>
                </div> */}
                
                <div className="exp-container-6 w-container"><br></br><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    <div className="the-div">
                        <h1 className="exp-the">the</h1>
                        <h1 className="exp-the-2">the</h1>
                        <h1 className="exp-the-2 the-2">the</h1>
                        <h1 className="exp-the-2 thee-3">the</h1>
                        <h1 className="exp-the-2 the-4">the</h1>
                    </div>
                    <div className="the-div space">
                        <h1 className="exp-the">space</h1>
                        <h1 className="exp-the-2">space</h1>
                        <h1 className="exp-the-2 the-2">space</h1>
                        <h1 className="exp-the-2 thee-3">space</h1>
                        <h1 className="exp-the-2 the-4">space</h1>
                    </div>
                </div><br></br><br></br>
                <div className="exp-div-6">
                    <h1 className="exp-heading-9">WE’RE ALL ABOUT THE COMMUNITY.</h1>
                    <a href="https://www.youtube.com/channel/UCTaWejLHHW8L5aG1IxQyWCw?guided_help_flow=5" target="_blank" className="youtube">youtube</a>
                    <a href="#" className="podcast">podcasts</a>
                    <a href="https://www.instagram.com/chalksnslate_media/" target="_blank" className="instagram">Instagram</a>
                    <img src="images/div.png" loading="lazy" sizes="100vw" srcset="images/div-p-500.png 500w, images/div-p-800.png 800w, images/div-p-1080.png 1080w, images/div-p-1600.png 1600w, images/div.png 1771w" alt="" />
                </div>

                <center>
                    <div>
                        <a href="#top"> <button type="button" className="btn btn-dark btnup" id="homeup">Scroll Up</button></a>
                    </div>
                </center>

            </div>





        </div>
    )
}

export default Creative