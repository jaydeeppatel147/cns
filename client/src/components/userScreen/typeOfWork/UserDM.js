import React from 'react'
import UserFooter from '../UserFooter'
import NavBar from "../../Navbar";

const UserDM = () => {
    return (
        <div>
            <NavBar></NavBar><br></br>
            <div className="branding-2">

                {/* main img */}
                {/* <div className="branding-img">
                    <img src="images/Website_thumbnails-02_1.png" loading="lazy" sizes="(max-width: 1920px) 100vw, 1920px" srcset="images/Website-thumbnails-01-1-p-500.png 500w, images/Website-thumbnails-01-1.png 1920w" alt="" />
                </div> */}

                <div class="containerImg">
                    <img src="images/branding1.jpg" alt="Snow" sizes="(max-width: 1920px) 100vw, 1920px" />

                    <div class="centeredImg">

                        <div className="branding2">
                            <br></br><br></br>
                            <b className="textImg">Digital Marketing</b>
                        </div>


                    </div>
                </div>

                <div className="branding-container w-container">
                    <div className="w-layout-grid grid-12">
                        <div>
                            <h1 className="heading-24">What we do</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">Services</h2>
                            <p className="paragraph-5"> Using data analytics to understand your target audience & how to approach them is the crux of our digital marketing strategy. Search engine optimization, along with aggressive Social Media campaigns is how we appeal to them.
</p>
                            <h2 className="heading-25">Roles &amp; responsibility</h2>
                            <p className="paragraph-5">It is our responsibility to ensure that your brand’s digital presence is felt by your target audience. We brainstorm and come up with the perfect social media calendar, which will leave an indelible impression on those you wish to attract. CNS ensures that your brand’s digital presence is perpetual & unquestionable.
 </p>
                        </div>
                        <div>
                            <h1 className="heading-24">Process</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">How we do things</h2>
                            <p className="paragraph-5">CNS’ talented team of copywriters & designers will provide you with a few sample posts & digital marketing strategies. Once they are approved, we get to work and create an entire calendar, which will be strictly adhered to, thereby making sure that your brand’s social media platforms are never lacking fresh content.
</p>
                        </div>
                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default UserDM