import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Detail = () => {
  return (
    <div>
      <NavBar></NavBar><br></br><br></br>
    <div data-ix="page-loading" className="page-wrapperh1">
      <div className="page-header-sectionh1 blogh1">
        <h1 data-ix="fade-up-1">Blog</h1>
      </div>
      <div className="sectionh1 ">
        <div className="wrapperh1 w-containerh1">
          <div className="blog-posts-and-sidebarh1">
            <div className="row ">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div
                  className="categoryh1 post-card-v1-categoryh1"
                  style={{ marginRight: "29%" }}
                >
                  This is some text inside of a div block.
                </div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  // srcset="images/autumn-goodman-242822_1-p-1080.jpeg 1080w, images/autumn-goodman-242822_1-p-1600.jpeg 1600w, images/autumn-goodman-242822_1.jpg 2000w"
                  src="images/blog/1.png"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4
                className="post-card-v1-headerh1"
                style={{ marginLeft: "10%" }}
              >
                Voice search SEO Optimization
              </h4>
              <p className="post-card-v1-texth1" style={{ marginLeft: "10%" }}>
                In the year 2000, who could’ve predicted that social media
                <br /> will reach to this level, and that brands will be using
                it to market their products.
              </p>
              <div
                className="post-card-v1-bottomh1"
                style={{ marginLeft: "10%" }}
              >
                <Link to='user-blog_Authors'>
                  <a href="" className="post-card-v1-authorh1 w-inline-blockh1">
                    <img
                      src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                      alt=""
                      className="post-card-v1-avatarh1"
                    />
                    <div>Divyansh</div>
                  </a>
                </Link>
                <div className="blog-card-v1-doth1"></div>
                {/* <div className="text-block-5h1">Published on %th Dec</div> */}
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrow w-inline-blockh1"
                >
                  <div
                    className="link-arrow-texth1"
                    style={{ marginLeft: "475px" }}
                  >
                    <Link to="/user-seo" className="mx-3">Read More</Link>

                  </div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>

            {/* ---------------------------------------------Blog2------------------------------------------------------------------------------------------------- */}

            <div className="row ">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div
                  className="categoryh1 post-card-v1-categoryh1"
                  style={{ marginRight: "29%" }}
                >
                  This is some text inside of a div block.
                </div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  // srcset="images/autumn-goodman-242822_1-p-1080.jpeg 1080w, images/autumn-goodman-242822_1-p-1600.jpeg 1600w, images/autumn-goodman-242822_1.jpg 2000w"
                  src="images/blog/3.jpg"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4
                className="post-card-v1-headerh1"
                style={{ marginLeft: "10%" }}
              >
                CULT BRANDING
              </h4>
              <p className="post-card-v1-texth1" style={{ marginLeft: "10%" }}>
                A cult is a group of people who have a common interest - be it
                religious, spiritual, <br></br>a philosophical belief pertaining
                to a particular object, personality or
              </p>
              <div
                className="post-card-v1-bottomh1"
                style={{ marginLeft: "10%" }}
              >
                <Link to='user-blog_Authors'>
                  <a href="#" className="post-card-v1-authorh1 w-inline-blockh1">
                    <img
                      src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                      alt=""
                      className="post-card-v1-avatarh1"
                    />
                    <div>Divyansh</div>
                  </a>
                </Link>
                <div className="blog-card-v1-doth1"></div>
                {/* <div className="text-block-5h1">Published on %th Dec</div> */}
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrow w-inline-blockh1"
                >
                  <div
                    className="link-arrow-texth1"
                    style={{ marginLeft: "475px" }}
                  >
                    <Link to="/user-cult_branding" className="mx-3">Read More</Link>
                  </div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>

            {/* ---------------------------------------------Blog3------------------------------------------------------------------------------------------------- */}

            <div className="row ">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div
                  className="categoryh1 post-card-v1-categoryh1"
                  style={{ marginRight: "29%" }}
                >
                  This is some text inside of a div block.
                </div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  // srcset="images/autumn-goodman-242822_1-p-1080.jpeg 1080w, images/autumn-goodman-242822_1-p-1600.jpeg 1600w, images/autumn-goodman-242822_1.jpg 2000w"
                  src="images/blog/4.jpg"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4
                className="post-card-v1-headerh1"
                style={{ marginLeft: "90px" }}
              >
                Digital marketing: the new normal
              </h4>
              <p className="post-card-v1-texth1" style={{ marginLeft: "90px" }}>
                Gone are the days when people sent letters to each other. Now,
                everyone can connect with anyone in seconds, via a call or by a
                text.
                <br /> There are more mobile
              </p>
              <div
                className="post-card-v1-bottomh1"
                style={{ marginLeft: "90px" }}
              >
                <Link to='user-blog_Authors'>
                  <a href="#" className="post-card-v1-authorh1 w-inline-blockh1">
                    <img
                      src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                      alt=""
                      className="post-card-v1-avatarh1"
                    />
                    <div>Divyansh</div>
                  </a>
                </Link>

                <div className="blog-card-v1-doth1"></div>
                {/* <div className="text-block-5h1">Published on %th Dec</div> */}
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrow w-inline-blockh1"
                >
                  <div
                    className="link-arrow-texth1"
                    style={{ marginLeft: "475px" }}
                  >
                    <Link to="/user-digital_marketing" className="mx-3">Read More</Link>
                  </div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>

            <div className="row ">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div
                  className="categoryh1 post-card-v1-categoryh1"
                  style={{ marginRight: "29%" }}
                >
                  This is some text inside of a div block.
                </div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  // srcset="images/autumn-goodman-242822_1-p-1080.jpeg 1080w, images/autumn-goodman-242822_1-p-1600.jpeg 1600w, images/autumn-goodman-242822_1.jpg 2000w"
                  src="images/blog/5.jpg"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4
                className="post-card-v1-headerh1"
                style={{ marginLeft: "90px" }}
              >
                Moment Marketing & how it improves audience engagement
              </h4>
              <p className="post-card-v1-texth1" style={{ marginLeft: "90px" }}>
                DIGITAL MARKETING is always evolving and moment marketing is{" "}
                <br />
                something that has recently come into force. Moment is a very
                brief period of time...
              </p>
              <div
                className="post-card-v1-bottomh1"
                style={{ marginLeft: "90px" }}
              >

                <Link to='user-blog_Authors'>
                  <a href="#" className="post-card-v1-authorh1 w-inline-blockh1">
                    <img
                      src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                      alt=""
                      className="post-card-v1-avatarh1"
                    />
                    <div>Divyansh</div>
                  </a>
                </Link>
                <div className="blog-card-v1-doth1"></div>
                {/* <div className="text-block-5h1">Published on %th Dec</div> */}
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrow w-inline-blockh1"
                >
                  <div
                    className="link-arrow-texth1"
                    style={{ marginLeft: "475px" }}
                  >
                    <Link to="/user-moment_marketing" className="mx-3">Read More</Link>
                  </div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>

            <div className="row ">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div
                  className="categoryh1 post-card-v1-categoryh1"
                  style={{ marginRight: "29%" }}
                >
                  This is some text inside of a div block.
                </div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  // srcset="images/autumn-goodman-242822_1-p-1080.jpeg 1080w, images/autumn-goodman-242822_1-p-1600.jpeg 1600w, images/autumn-goodman-242822_1.jpg 2000w"
                  src="images/blog/12.jpg"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4
                className="post-card-v1-headerh1"
                style={{ marginLeft: "90px" }}
              >
                A guide to influencer marketing
              </h4>
              <p className="post-card-v1-texth1" style={{ marginLeft: "90px" }}>
                If you seek a way to grab the attention of your customers, you
                shouldn’t look beyond someone <br />
                who already has their attention.
              </p>
              <div
                className="post-card-v1-bottomh1"
                style={{ marginLeft: "90px" }}
              >

                <Link to='user-blog_Authors'>
                  <a href="#" className="post-card-v1-authorh1 w-inline-blockh1">
                    <img
                      src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                      alt=""
                      className="post-card-v1-avatarh1"
                    />
                    <div>Divyansh</div>
                  </a>
                </Link>

                <div className="blog-card-v1-doth1"></div>
                {/* <div className="text-block-5h1">Published on %th Dec</div> */}
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrow w-inline-blockh1"
                >
                  <div
                    className="link-arrow-texth1"
                    style={{ marginLeft: "475px" }}
                  >
                    <Link to="/user-influncer_marketing" className="mx-3">Read More</Link>
                  </div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>

            <div className="row ">
              <a
                href="#"
                data-ix="zoom-image"
                className="post-card-v1-imageh1 w-inline-blockh1"
              >
                <div
                  className="categoryh1 post-card-v1-categoryh1"
                  style={{ marginRight: "29%" }}
                >
                  This is some text inside of a div block.
                </div>
                <img
                  sizes="(max-width: 479px) 87vw, (max-width: 767px) 53vw, 54vw"
                  // srcset="images/autumn-goodman-242822_1-p-1080.jpeg 1080w, images/autumn-goodman-242822_1-p-1600.jpeg 1600w, images/autumn-goodman-242822_1.jpg 2000w"
                  src="images/blog/10.png"
                  alt=""
                  className="zoom-on-hoverh1"
                />
              </a>
              <h4
                className="post-card-v1-headerh1"
                style={{ marginLeft: "90px" }}
              >
                Marketing in camouflage: Native Advertising
              </h4>
              <p className="post-card-v1-texth1" style={{ marginLeft: "90px" }}>
                No one wants to see an advertisement in the middle of something,
                <br /> so now, it’s a challenging task for advertising agencies
                to capture the users’
              </p>
              <div
                className="post-card-v1-bottomh1"
                style={{ marginLeft: "90px" }}
              >

                <Link to='user-blog_Authors'>
                  <a href="#" className="post-card-v1-authorh1 w-inline-blockh1">
                    <img
                      src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                      alt=""
                      className="post-card-v1-avatarh1"
                    />
                    <div>Divyansh</div>
                  </a>
                </Link>
                <div className="blog-card-v1-doth1"></div>
                {/* <div className="text-block-5h1">Published on %th Dec</div> */}
                <a
                  href="#"
                  data-ix="more-link"
                  className="link-arrow w-inline-blockh1"
                >
                  <div
                    className="link-arrow-texth1"
                    style={{ marginLeft: "475px" }}
                  >
                    <Link to="/user-native_advertising" className="mx-3">Read More</Link>
                  </div>
                  <img
                    src="images/link-arrow-icon.svg"
                    alt=""
                    className="link-arrow-iconh1"
                  />
                </a>
              </div>
            </div>

            <div
              data-ix="fade-left"
              data-w-id="4a62ac2b-c8aa-2fe6-e58b-c88f4a5126cb"
              style={{ opacity: 0 }}
              className="blog-sidebarh1"
            >
              {/* form */}

              {/* <div className="w-formh1">
                <form
                  id="Blog-Subscribe-Form"
                  name="wf-form-Subscribe-Form"
                  data-name="Subscribe Form"
                  className="sidebar-subscribe w-hidden-tinyh1"
                >
                  <h5 className="sidebar-subscribe-headerh1">
                    Subscribe to get latest posts straight to your inbox
                  </h5>
                  <input
                    type="email"
                    id="email-3"
                    name="email"
                    data-name="Email 3"
                    placeholder="Enter your email address"
                    maxlength="256"
                    required=""
                    className="input w-inputh1"
                  />
                  <input
                    type="submit"
                    value="Subscribe"
                    data-wait="Please wait..."
                    className="buttonh1 sidebar-subscribe-buttonh1 w-buttonh1"
                  />
                </form>
                <div className="w-form-doneh1">
                  <div>Thank you! Your submission has been received!</div>
                </div>
                <div className="w-form-failh1">
                  <div>
                    Oops! Something went wrong while submitting the form
                  </div>
                </div>
              </div> */}
              {/* <div className="blog-sidebar-blockh1">
                <h5 className="sidebar-headerh1">Categories</h5>
                <a href="#" className="sidebar-categoryh1 w-inline-blockh1">
                  <div className="blog-category-doth1"></div>
                  <div>Design</div>
                </a>
              </div> */}
              {/* <div className="blog-sidebar-blockh1">
                <h5 className="sidebar-headerh1">Tags</h5>
                <a href="#" className="sidebar-tagh1">
                  Text Link
                </a>
              </div> */}
              {/* <div className="blog-sidebar-blockh1 w-hidden-tinyh1">
                <h5 className="sidebar-headerh1">Featured Posts</h5>
                <a
                  href="#"
                  className="sidebar-featured-post-cardh1 w-inline-blockh1"
                >
                  <div className="sidebar-featured-post-imageh1">
                    <img
                      src="images/mockup-iphone-preview.png"
                      alt=""
                      className="zoom-on-hoverh1"
                    />
                  </div>
                  <div className="sidebar-featured-post-categoryh1">
                    MARKETING
                  </div>
                  <h6 className="sidebar-featured-post-headerh1">
                    Effects of using the mobile
                  </h6>
                </a>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Detail;
