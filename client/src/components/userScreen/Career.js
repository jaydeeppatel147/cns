import React, { Component, useState, useEffect, useContext } from "react";
import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import Footer from './UserFooter'
import NavBar from '../Navbar'



const Career = () => {
  var [name, setName] = useState("");
  var [number, setNumber] = useState("");
  var [email, setEmail] = useState("");
  var [like_to_do, setLike_to_do] = useState("");
  // var [upload_CV, setImage] = useState("");
  var [portfolio, setportfolio] = useState("");
  var [message, setmessage] = useState("");
  var PostData = () => {
    // const data = new FormData();
    // console.log(name, number, email)
    // data.append("file", upload_CV);
    fetch("/career", {
      method: "post",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name,
        email,
        number,
        like_to_do,
        // upload_CV,
        portfolio,
        message
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data.user);
        if (data.error) {
          M.toast({
            html: "PLEASE ENTER DETAILS ",
            classes: "#D50000 red accent-4"
          });
        } else {
          // localStorage.setItem("jwt",data.token);
          // localStorage.setItem("user", JSON.stringify(data.user));
          // dispatch({type:"USER",payload:data.user})
          M.toast({
            html: " Data Posted successfully",
            classes: "#4CAF50 green"
          });
          //history.push("/");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

//   const handleChange = (e) => {
//     // setProgess(0)
//     const file = e.target.files[0]; // accesing file
//     console.log(file);
//     setupload_CV(file); // storing file
// }

  return (
      <div>
          <NavBar></NavBar>
      <br></br>
    <div className="section-2">
      <div className="columns-2 w-row">
        <div className="column-10 w-col w-col-6 w-col-stack">
          <div className="div-block-4">
            <h1 className="heading-28">Put Your Career On Right Track</h1>
            {/* <p className="paragraph-6">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse varius enim in eros elementum tristique. Duis cursus,
              mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam
              libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum
              lorem imperdiet. Nunc ut{" "}
            </p> */}
            <div className="branding-grid">
              <div className="w-layout-grid grid-13">
                <a href="#" class="branding-link-block w-inline-block">
                  <img
                    src="images/bookmark-line.png"
                    loading="lazy"
                    alt=""
                    class="image-9"
                  />
                </a>
                <h5 className="heading-29">
                  2nd Floor, No. 34/3, 1st Main Roadkoramangala 1st block,
                  Bengaluru - 560034
                </h5>
                <a href="#" className="branding-link-block w-inline-block">
                  <img
                    src="images/phone-line.png"
                    loading="lazy"
                    alt=""
                    class="image-9"
                  />
                </a>
                <h5 className="heading-29">9717936200</h5>
                <a href="#" className="branding-link-block w-inline-block">
                  <img
                    src="images/mail-line.png"
                    loading="lazy"
                    alt=""
                    class="image-9"
                  />
                </a>
                <h5 className="heading-29">careers@chalksnslate.com</h5>
              </div>
            </div>
            <div className="div-block-6">
              <a href="#" className="w-inline-block">
                <img
                  src="images/instagram.png"
                  loading="lazy"
                  alt=""
                  class="branding-insta"
                />
              </a>
              <a href="#" className="w-inline-block">
                <img
                  src="images/linkedin.png"
                  loading="lazy"
                  alt=""
                  class="branding-linkedin"
                />
              </a>
              <a href="#" className="w-inline-block">
                <img
                  src="images/twitter.png"
                  loading="lazy"
                  alt=""
                  class="branding-twitter"
                />
              </a>
            </div>
          </div>
        </div>
        <div className="column-11 w-col w-col-6 w-col-stack">
          <div className="div-block-5">
            <div className="career-form-block w-container">
              <div className="branding-form-header">
                <div className="text-block-4">
                  Fill in the form OR{" "}
                  <span className="contact-span">Send us an email</span>
                </div>
              </div>
              <div className="form-block contact-form w-form">
                <form id="email-form" name="email-form" data-name="Email Form">
                  <input
                    type="text"
                    className="text-field contact-tf w-input"
                    maxlength="256"
                    name="name"
                    data-name="Name 2"
                    placeholder="What&#x27;s your Name?"
                    id="name-2"
                    onChange={e => setName(e.target.value)}
                    value={name}
                  />
                  <input
                    type="email"
                    className="text-field-2 contact-tf w-input"
                    maxlength="256"
                    name="email"
                    data-name="Email 2"
                    placeholder="Your Email?"
                    id="email-2"
                    required=""
                    onChange={e => setEmail(e.target.value)}
                    value={email}
                  />
                  <input
                    type="tel"
                    className="text-field contactt-tf w-input"
                    maxlength="256"
                    name="number"
                    data-name="Phone 2"
                    placeholder="Your Phone number?"
                    id="phone-2"
                    required=""
                    onChange={e => setNumber(e.target.value)}
                    value={number}
                  />
                  <input
                    type="text"
                    className="text-field contactt-tf w-input"
                    maxlength="256"
                    name="like_to_do"
                    data-name="Profession"
                    placeholder="What would you like to do?"
                    id="Profession"
                    required=""
                    onChange={e => setLike_to_do(e.target.value)}
                    value={like_to_do}
                  />
                  {/* <input
                    type="file"
                    className="text-field contactt-tf w-input"
                    maxlength="256"
                    name="upload_CV"
                    data-name="CV"
                    placeholder="Attach Your CV"
                    id="CV"
                    onChange={(e) => setImage(e.target.files[0])}
                    value={upload_CV}
                  /> */}
                  <input
                    type="text"
                    className="text-field contactt-tf w-input"
                    maxlength="256"
                    name="portfolio"
                    data-name="Portfolio 2"
                    placeholder="Add Your Portfolio link or Linked in link"
                    id="Portfolio-2"
                    onChange={e => setportfolio(e.target.value)}
                    value={portfolio}
                  />
                  <textarea
                    placeholder="Message"
                    maxlength="5000"
                    id="Message-2"
                    name="message"
                    data-name="Message 2"
                    class="textarea text-field contact-tf w-input"
                    onChange={e => setmessage(e.target.value)}
                    value={message}
                  ></textarea>
                  <button
                    type="submit"
                    data-wait="Please wait..."
                    value="Submit"
                    class="submit-button textarea branding-submit w-button"
                    onClick={() => PostData()}
                  >
                    Submit
                  </button>
                </form>
              
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <Footer></Footer>
    </div>
  );
};
export default Career;