import React from 'react'
import NavBar from '../../Navbar'
import Footer from '../UserFooter'


const Kns = () => {
    return (
        <div>
            <NavBar></NavBar>
            <div className="container">
                <div className="">
                    <div className="containerci w-containerci">
                        <div className="div-block-2ci">
                            <h1>KNS</h1>
                            <div>Website Design & Social media management</div>
                        </div><br></br><br></br>
                        <div className="div-blockci">
                            <p className="paragraphci">KNS Infrastructure is a Market Leader in Plotted Layout Development. Every residential project comes with modern amenities & luxurious landscapes.

                            <br></br>
                            What we do: We developed KNS’ website & developed a social media strategy/calendar to promote their projects and draw the right audience.





</p>
                        </div>
                    </div>

                    <div className="rowj">
                    <div className="columnj">
                        <img src="images/kns/1.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        {/* <img src="images/unimech/2.png" className="m-3 p-3" style={{width:'100%'}} /> */}
                        

                    </div>
                    <div className="columnj">
                        <img src="images/kns/2.png" className="m-3 p-3" style={{width:'100%'}} />
                        {/* <img src="images/agromatic/4.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                        

                    </div>
                    <div className="columnj">
                        {/* <img src="images/unimech/4.png" className="m-3 p-3" style={{width:'100%'}} /> */}
                        {/* <img src="images/agromatic/6.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                       

                    </div>
                    <div className="columnj">
                        {/* <img src="images/unimech/5.png" className="m-3 p-3" style={{width:'100%'}} /> */}
                        {/* <img src="images/agromatic/8.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                        

                    </div>
                    
                </div>

                </div>
                
            </div>
            <div style={{marginTop: '0%'}}>
            <Footer></Footer>
            </div>
            
        </div>
    )
}

export default Kns