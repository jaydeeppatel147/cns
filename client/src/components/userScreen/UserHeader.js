import React from 'react'

const UserHeader = () => {
    return(
        <div>
            <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" className="navbar-2 w-nav">
             <div className="container w-container">
      <a href="#" className="brand-2 w-nav-brand"><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fdc6a1df4732d903f276124_LogoCNS.png" loading="lazy" alt="" class="logo" /></a>
      <nav role="navigation" className="nav-menu-2 w-nav-menu">
        <a href="about-us.html" className="nav-link w-nav-link">About Us</a>
        <a href="our-works.html" className="nav-link-2 w-nav-link">Our Works</a>
        <a href="#" className="nav-link-3 w-nav-link">Community</a>
        <a href="experimental.html" className="nav-link-4 w-nav-link">Blog</a>
        <a href="contact-us.html" className="button-6 w-button">Contact Us</a>
      </nav>
      <div className="menu-button w-nav-button">
        <div className="icon-2 w-icon-nav-menu"></div>
      </div>
    </div>
  </div>
        </div>
    )
}

export default UserHeader