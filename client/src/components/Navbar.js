import React, { Component, useContext, useRef, useEffect, useState } from 'react';
// import Profile from './screens/Profile';
import { Link, useHistory } from "react-router-dom";

import { UserContext } from './../App';
// import ScrapRequest from './screens/ScrapRequest';
// import M from 'materialize-css'
// import AllProject from './screens/AllProject'

const NavBar = () => {
  const searchModal = useRef(null)
  const [search, setSearch] = useState('')
  const [userDetails, setUserDetails] = useState([])
  const { state, dispatch } = useContext(UserContext)
  const history = useHistory()
  useEffect(() => {
    // M.Modal.init(searchModal.current)
  }, [])


  var renderList = () => {
    if (state) {
      return [
        <div >

        
        <Link to="/admin-home" className="mx-3">All Project</Link>
        <Link to="/profileblog" className="mx-3">All Blog</Link>
        <Link to="/createproject" className="mx-3">Create Project</Link>
        <Link to="/createblog" className="mx-3">Create Blog</Link>
        <Link to="/user-signup" className="mx-3">SignUp Admin</Link>




        <button
            className="btn btn-dark bn-large"
            onClick={() => {
              localStorage.clear()
              dispatch({ type: "CLEAR" })
              history.push('/user-home')
            }}
          >
            LogOut
      </button></div>
      ]

    } else {
      return [


        <Link to="/user-home" className="mx-3" style={{ textDecoration: 'none' }}><b>Home</b></Link>,
       <Link to="/user-aboutus" className="mx-3" style={{ textDecoration: 'none' }}><b>About Us</b></Link>,
       <Link to="/user-work" className="mx-3" style={{ textDecoration: 'none' }}><b>Our Work</b></Link>,
        <Link to="/user-bloguser" className="mx-3" style={{ textDecoration: 'none' }}><b>Blogs</b></Link>,
      //  <Link to="/user-projectuser" className="mx-3"><b>Projects</b></Link>,
        
        // <Link to="/user-signin" className="mx-3" style={{ textDecoration: 'none' }}><b>Admin Signin</b></Link>,
        // <Link to="/user-signup" className="mx-3"><b>Admin Signup</b></Link>,
        <Link to="/user-contact" className="mx-3"><button type="button" class="btn btn-dark"><b>Contact Us</b></button></Link>


      ]

    }

    
  }



  

  return (
    // <nav>
    //     <div className="nav-wrapper header-bg" >
    //       <Link to={state?"/" : "/home"} className="brand-logo left ml-5" >
    //       <img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fdc6a1df4732d903f276124_LogoCNS.png" loading="lazy" alt="" class="logo" />
    //       </Link>


    //       <ul id="nav-mobile" className="right mr-5 pr-5">
    //         {renderList()}
    //       </ul>

    //     </div>

    //   </nav>


    <div>
      <div data-collapse="medium" data-animation="default" style={{backgroundColor:'white'}} data-duration="400" role="banner" className="navbar-2 w-nav">
        <div className="container w-container header-bg" style={{backgroundColor:'white'}}>
          <a href="/user-home" className="brand-2 w-nav-brand" style={{backgroundColor:'white', }}><img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fdc6a1df4732d903f276124_LogoCNS.png" loading="lazy" alt="" class="logo" /></a>
          <nav role="navigation" className="nav-menu-2 w-nav-menu navShadow" style={{backgroundColor:'white'}}>
          {renderList()}
          </nav>
          <div className="menu-button w-nav-button" style={{backgroundColor:'white'}}>
            <div className="icon-2 w-icon-nav-menu" style={{backgroundColor:'white'}}></div>
          </div>
        </div>
      </div>
    </div>


  );
}

export default NavBar;