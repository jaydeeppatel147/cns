import React from 'react'
import NavBar from '../../Navbar'
import Footer from '../UserFooter'

const Agromatic = () => {
    return (
        <div>
            <NavBar></NavBar><br></br><br></br>

            <div className="section12">
                <div className="container12 w-container12">
                    <div className="div-block-212">
                        <h1>Upciclo</h1>
                        <div>Branding, Social Media and Content</div>
                    </div>
                    <div className="div-block12">
                        <p className="paragraph12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla,</p>
                    </div>
                </div>
                <div className="image-container12 w-container12">
                    <div className="image-holder _112"><img src="images/1.jpeg" loading="lazy" alt="" className="brainstorm" /></div>
                    <div className="image-holder _212"></div>
                    <div className="image-holder _312"></div>
                </div>
            </div>

            <div className="section12">
    <div className="container12 w-container12">
      <div className="div-block-212"></div>
      <div className="div-block12 combo12">
        <p className="paragraph12">&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere&quot;</p>
        <div className="text-block12">This is some text inside of a div block.</div>
      </div>
    </div>
    <div className="image-container12 w-container12">
      <div className="image-holder _412"></div>
      <div className="image-holder _512"></div>
      <div className="image-holder _612"></div>
    </div>
  </div>




            <Footer></Footer>
        </div>
    )
}

export default Agromatic