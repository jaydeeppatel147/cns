import React from 'react'

import NavBar from "../../Navbar";
import UserFooter from '../UserFooter'

const Animation = () => {
    return (
        <div>
            <NavBar></NavBar><br></br>
            <div className="branding-2">

                {/* main img */}
                {/* <div className="branding-img">
                    <img src="images/branding1.jpg" loading="lazy" sizes="(max-width: 1920px) 100vw, 1920px"  alt="" /><div className="center">srhsrh</div>
                </div> */}

                <div class="containerImg">
                    <img src="images/branding1.jpg" alt="Snow" sizes="(max-width: 1920px) 100vw, 1920px" />

                    <div class="centeredImg">

                        <div className="branding2">
                            <br></br><br></br>
                            <b className="textImg">ANIMATION</b>
                        </div>


                    </div>
                </div>

                <div className="branding-container w-container">
                    <div className="w-layout-grid grid-12">
                        <div>
                            <h1 className="heading-24">What we do</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">Services</h2>
                            <p className="paragraph-5">Developing a social media calendar, optimizing your posts to reach your target audience, coming up with innovative copies & designs to dazzle those who check out your profile on multiple platforms.
</p>
                            <h2 className="heading-25">Roles &amp; responsibility</h2>
                            <p className="paragraph-5">We handle everything from creating a calendar to the day-to-day functioning of your social media channels. We ensure that it is regularly updated & your audience is always hooked to what we have to say.

</p>
                        </div>
                        <div>
                            <h1 className="heading-24">Process</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">How we do things</h2>
                            <p className="paragraph-5">We start by coming up with a strategy & developing a social media calendar for your profiles on different platforms. After this, we follow the latest trends in the type of content which your audience will like, and keeping this at the heart of operations, we go about executing our calendar & strategy on the ground level.</p>
                        </div>
                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default Animation