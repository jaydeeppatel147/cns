const mongoose = require("mongoose");
const { Schema } = mongoose;

const contactSchema = new Schema({
    name:{type:String},
    number:{type:String},
    email:{type:String},
    txt:{type:String}
}, {timestamps : true})

const contact = mongoose.model("contact", contactSchema);
module.exports = contact;