import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Pulse from "react-reveal/Pulse";


const UserProject = () => {
    return (
        <div>
            <div className="section1">
                {/* <Pulse duration={3000}>
                    <img src="images/logo1.png" loading="lazy" data-w-id="b0c8c666-91c3-9c85-bb85-6459219cdaad" alt="" class="image1" />
                </Pulse> */}

                <div className="image1">
                    <div className="cardcontainer"><img className="galleryImg" src="images/bird.png" /></div>
                </div>

                {/* <a href="/creative#trial" className="button w-button">Cick anywhere on PINK</a> */}
                <a href="/user-creative" style={{ textDecoration: 'none' }}>

                    <div className="pink-partition1">
                        <div className="part-11">
                            <h1 className="heading1">Creative &nbsp;&nbsp;</h1>
                        </div>
                        <div className="part-21">
                            <p className="paragraph1">We take all the risks,
you just sit back and reap the rewards!</p>
                            {/* <a href="#" className="button w-button">Cick anywhere on PINK</a> */}
                            <button type="button" className="btn btn-dark">Click Anywhere in PINK</button>
                        </div>
                    </div>
                </a>

                <Link to="/user-home" style={{ textDecoration: 'none' }}>
                    <div className="yellow-partition1">
                        <div className="part-11">
                            <h1 className="heading1 combo1">Selective</h1>
                        </div>
                        <div className="part-21">
                            <p className="paragraph1 combo1">Brainstorm Ideas
Create them Faster </p>
                            {/* <a href="#" className="button w-button">Click anywhere on YELLOW</a> */}
                            <button type="button" className="btn btn-dark">Click Anywhere in YELLOW</button>
                        </div>
                    </div>
                </Link>
            </div>
        </div>
    )
}

export default UserProject