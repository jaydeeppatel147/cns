import React from 'react'
import UserFooter from './UserFooter'
// import UserHeader from './UserHeader'
import MiniContactForm from './MiniContactForm'
import { Link, useHistory } from "react-router-dom";
import animationData1 from './lottie/mascot_earth.json';
import animationData2 from './lottie2/twig pickup.json';
import animationData3 from './lottie2/twig_nest.json';
import Lottie from 'react-lottie';
// import Img1 from './img/1.jpeg'
import VideoHome from './video/homevideo.mp4'
import ReactPlayer from "react-player";
import NavBar from "../Navbar";
import Zoom from "react-reveal/Zoom";
import Fade from "react-reveal/Fade";

const Home = () => {

    

    const defaultOptions1 = {
        loop: true,
        autoplay: true,
        animationData: animationData1,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    };

    const defaultOptions2 = {
        loop: true,
        autoplay: true,
        animationData: animationData2,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    };

    const defaultOptions3 = {
        loop: true,
        autoplay: true,
        animationData: animationData3,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    };


    return (
        <div>
            <NavBar></NavBar>
            {/* <UserHeader></UserHeader> */}
            <div className="body">

                {/* video section */}
                {/* <div className="section123">
                    <div className="container-2 w-container"> */}

                {/* <iframe width="100%" height="455" src={VideoHome} allow='autoplay; encrypted-media'
        allowFullScreen>
                        </iframe> */}
                {/* <div className="embed-responsive embed-responsive-16by9" style={{margin: '-6% 0 -5% 0'}}>
                            <iframe className="embed-responsive-item" src={VideoHome} 
                             frameBorder='0'
                             allow='autoplay; encrypted-media'
                             allowFullScreen
                             title='video'></iframe>
                             
                        </div> */}

                {/* <img src="https://uploads-ssl.webflow.com/5fdc6873a46f702548db6eb4/5fdc7bda4467d535b7d5963c_video%20placeholder.png" loading="lazy" alt="" >
                         
                        hvhv
                             
                         </img> */}
                {/* </div>
                </div> */}

                {/* img + video */}
                <div>
                    <div id="example1">

                        <video width="300" controls={false} autoplay="autoplay" autoPlay={true} loop>
                            <source src={VideoHome} type="video/mp4" />
                        </video>


                    </div>
                </div>

                {/* about us section */}

                <div className="section about-us-section">
                    <div className="container-3 w-container">
                        <div className="motto">
                            <div className="label">We know you have lots of questions.</div>
                            <div className="label">You want to know who we are and What we do?</div>
                            <img src="images/homeimg1.png" loading="lazy" alt="" />
                            <div className="text-block">We are the People Bold Enough, To do Viral things with your Brand</div>
                        </div>
                        <div className="aboutus-content">
                            <div className="w-layout-grid about-grid">
                                <div id="w-node-e11df53262e2-1c6885d9" className="div-block-2">
                                    <div className="about-section">
                                        <div className="label about-label">What we just claimed was very BOLD. Now how do we do it?</div>
                                        <h2 className="text-block about-heading">By <span className="text-span">creating</span> super-unique Brand Identities.</h2>
                                        <div className="label about-label-2">for the people/brands who are willing to be different and stand out, obviously in a good way.</div>
                                    </div>
                                    <Link to="/user-work">
                                        <div className="button-5 w-button">See our Works</div>
                                    </Link>
                                </div>
                                {/* <img src="hsttps://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142d3d507b902a_Screenshot%202020-11-21%20at%201.18.54%20PM.png" loading="lazy" id="w-node-e11df53262ef-1c6885d9" alt="" /> */}
                                <Lottie
                                    options={defaultOptions2}
                                // height={400}
                                // width={400}
                                />
                            </div>
                            <div className="w-layout-grid about-grid aboutmid">
                                {/* <img src="https://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142d676a7b902b_Screenshot%202020-11-21%20at%201.19.04%20PM.png" loading="lazy" id="w-node-e11df53262f1-1c6885d9" alt="" /> */}
                                <Lottie
                                    options={defaultOptions3}
                                // height={400}
                                // width={400}
                                />
                                <div id="w-node-e11df53262f2-1c6885d9" className="div-block-2">
                                    <div className="about-section">
                                        <div className="label about-label">What we just claimed was very BOLD. Now how do we do it?</div>
                                        <h2 className="text-block about-heading">By <span class="text-span">building</span> Cutting - edge Websites.</h2>
                                        <div className="label about-label-2">for the people/brands who are willing to be different and stand out, obviously in a good way.</div>
                                    </div>
                                    <Link to="/user-work">
                                        <div className="button-5 w-button">See our Works</div>
                                    </Link>
                                </div>
                            </div>
                            <div className="w-layout-grid about-grid">
                                <div id="w-node-e11df5326300-1c6885d9" className="div-block-2">
                                    <div className="about-section">
                                        <div className="label about-label">What we just claimed was very BOLD. Now how do we do it?</div>
                                        <h2 className="text-block about-heading">By <span className="text-span">growing</span> your Social Media Presence.</h2>
                                        <div className="label about-label-2">for the people/brands who are willing to be different and stand out, obviously in a good way.</div>
                                    </div>
                                    <Link to="/user-work">
                                        <div className="button-5 w-button">See our Works</div>
                                    </Link>
                                </div>
                                {/* <img src="https://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142dfb8d7b902c_Screenshot%202020-11-21%20at%201.19.13%20PM.png" loading="lazy" alt="" /> */}
                                <Lottie
                                    options={defaultOptions1}
                                // height={400}
                                // width={400}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                {/* community */}

                <div className="community">
                    <div className="testimonials w-container">
                        {/* <a href="#" className="small small-first w-inline-block" />
                        <div className="small-image"></div>
                        <a href="#" className="small w-inline-block" />
                        <div className="small-image small2"></div> */}

                        {/* testimonials */}

                        {/* <a href="#" className="medium w-inline-block" />
                        <div className="medium-image medium1 portfolio-item"></div>
                        
                        <a href="#testimonial1" className="big w-inline-block" />
                        <div className="image portfolio-item"></div>
                        
                        <a href="#tesimonial2" className="medium w-inline-block" />
                        <div className="medium-image medium-image2 portfolio-item"></div> remove br */}

<section id="testim" className="testim" style={{marginTop:'10%'}}>
                <div className="testim-cover">
                    <div className="wrap">
        
                        <span id="right-arrow" className="arrow right fa fa-chevrxon-right"></span>
                        <span id="left-arrow" className="arrow left fa fa-chexvron-left "></span>
                        <ul id="testim-dots" className="dots">
                            <li className="dot active"></li>
                            
                            <li className="dot"></li>
                            <li className="dot"></li>
                        </ul>
                        <div id="testim-content" className="cont">
                            
                            <div className="active">
                                {/* <!-- <div class="img"><img src="https://image.ibb.co/hgy1M7/5a6f718346a28820008b4611_750_562.jpg" alt=""></div> --> */}
                                <div className="row">
                                    <div className="col">
                                        {/* <div className="img"><img src="images/1.jpg" alt="" /></div> */}
                                    </div>
                                    <div className="col">
                                        <div className="img"><img src="images/2.jpeg" alt="" /></div>
                                    </div>
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t33.png" alt="" /></div> */}
                                    </div>
                                    

                                </div>
                                <h2>Vishal</h2>
                                <p style={{color: 'black'}}>We at Agromatic are highly impressed with the creativity that CNS has brought out in re-designing our in-house communication tools packaging. CNS exceeded our expectation, in terms of both quality of work as well as promptness of deliverables</p>                    
                            </div>
        
                            <div>
                                <div className="row">
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t2.jpeg" alt="" /></div> */}
                                    </div>
                                    <div className="col">
                                        <div className="img"><img src="images/t33.png" alt="" /></div>
                                    </div>
                                    <div className="col">
                                        {/* <div className="img"><img src="images/1.jpg" alt="" /></div> */}
                                    </div>
                                    

                                </div>
                                <h2>Abhishek</h2>
                                <p style={{color:'black'}}>Chalksnslate is a company that walks the talk. They do believe that relationships are key to the business… It is one of our closest partners and is one that is really really looking at ways to improve the product and relationship.</p>                    
                            </div>

                            <div>
                                <div className="row">
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t33.png" alt="" /></div> */}
                                    </div>
                                    <div className="col">
                                        <div className="img"><img src="images/1.jpg" alt="" /></div>
                                    </div>
                                    <div className="col">
                                        {/* <div className="img"><img src="images/t2.jpeg" alt="" /></div> */}
                                    </div>
                                    

                                </div>
                                <h2>Vendy's</h2>
                                <p style={{color:'black'}}>I was wishing to add some pretty pictures ,and a video,  onto my website, and for that, I entrusted the job to Justin.
Justin, who assured me the work will be done well.
 I was not disappointed , and in the end , had some good theme based pictures, and a good video as well.
Will like to recommend ChalksnSlate Media for such  a requirement, in future.</p>                    
                            </div>
        
                           
        
                            
        
                            
        
                        </div>
        
                    </div>
                </div>
            </section><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>













                        {/* <a href="#" className="small w-inline-block" />
                        <div className="small-image small3"></div>
                        <a href="#" className="small small-last w-inline-block" />
                        <div className="small-image small4"></div> */}
                    </div>
                    {/* <div className="container-4 w-container">
                        <h2 className="heading3">Vishrut, KNS Group</h2>
                        <p className="paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere. vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet</p>
                    </div> */}
                    <div className="tree" style={{margin:'18% 0% 0% 0%'}}>
                        <img src="images/homegroup.png" loading="lazy" alt="" />
                    </div><br></br><br></br>
                    <div className="white-board w-container" >
                        <div className="board-column w-row">
                            <div className="columnleft w-col w-col-8">
                                <div className="left">
                                    <div className="left-content">
                                        <h2 className="board-heading">We&#x27;re also working on a community.</h2>
                                        <div className="text-block-2">With over 10+ Youtube videos, We have created free educational community to help people learn.</div>
                                    </div>
                                    <div className="w-layout-grid grid">
                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://www.youtube.com/channel/UCTaWejLHHW8L5aG1IxQyWCw?guided_help_flow=5" className="link-block w-inline-block" >
                                            <div className="data"><img src="images/youtube.png" loading="lazy" width="48" alt="" class="image-2" />
                                                <div className="datablock">
                                                    <h3 className="heading-3">Youtube</h3>
                                                    <div className="text-block-3">Talking about all things business, design, and coding.</div>
                                                </div>
                                            </div>
                                        </a>

                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://medium.com/chalksnslate-media" className="link-block w-inline-block" >
                                            <div id="w-node-3159b98a5947-1c6885d9" className="data"><img src="images/medium.png" loading="lazy" width="48" alt="" class="image-2" />
                                                <div className="datablock">
                                                    <h3 className="heading-3">Medium</h3>
                                                    <div className="text-block-3">Writing about our ideas and perspectives on design.</div>
                                                </div>
                                            </div>
                                        </a>

                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://www.linkedin.com/company/chalksnslate/?viewAsMember=true" className="link-block w-inline-block" >
                                            <div className="data"><img src="images/linkedin.png" loading="lazy" width="48" alt="" class="image-2" />
                                                <div className="datablock">
                                                    <h3 className="heading-3">Linkedin</h3>
                                                    <div className="text-block-3">Sharing knowledge, information and real-life experiences.</div>
                                                </div>
                                            </div>
                                        </a>

                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://www.instagram.com/chalksnslate_media/" className="link-block w-inline-block" >
                                            <div className="data"><img src="images/instagram.png" loading="lazy" width="48" alt="" class="image-2" />
                                                <div className="datablock">
                                                    <h3 className="heading-3">Instagram</h3>
                                                    <div className="text-block-3">Daily tips and tricks to inspire the Design Community.</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="column-right w-col w-col-4">
                                <div className="right">
                                    {/* <div className="w-layout-grid grid-4"><img src="https://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142d4d557b9038_Screenshot%202020-11-24%20at%2012.15.06%20AM.png" loading="lazy" width="250" alt="" className="_1" /><img src="https://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142dfbe27b903a_Screenshot%202020-11-24%20at%2012.15.23%20AM.png" loading="lazy" width="250" alt="" className="_2" /><img src="https://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142d0e6d7b903b_Screenshot%202020-11-24%20at%2012.15.43%20AM.png" loading="lazy" width="250" alt="" className="_3" /><img src="https://uploads-ssl.webflow.com/5feaef2506142d804b7b9012/5feaef2506142d6f6a7b9039_Screenshot%202020-11-24%20at%2012.15.14%20AM.png" loading="lazy" width="250" alt="" className="_4" /></div> */}
                                    <div className="w-layout-grid grid-4"><img src="images/cnsbird.jpg" loading="lazy" width="250" alt="" className="_1" /></div>
                                </div>
                            </div>
                        </div>
                        <div className="board-div"></div>
                    </div>
                </div>

                {/* contact form */}



                <MiniContactForm></MiniContactForm>
                <UserFooter></UserFooter>
            </div>
        </div>


    )
}

export default Home