import React, { Component, useState, useContext } from "react";
import M from "materialize-css";
import UserFooter from './UserFooter'
import NavBar from '../Navbar'

const Contact = () => {

    var [name, setName] = useState("");
    var [number, setNumber] = useState();
    var [email, setEmail] = useState("");
    var [txt, setTxt] = useState("");
    console.log(name,number)
  
  
    var PostData = () => {
      fetch("/contact_Us", {
        method: "post",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name,
          email,
          number,
          txt
        })
      })
        .then(res => res.json())
        .then(data => {
          console.log(data.user);
          if (data.error) {
            M.toast({ html: data.error, classes: "#D50000 red accent-4" });
          } else {
            // localStorage.setItem("jwt",data.token);
            // localStorage.setItem("user", JSON.stringify(data.user));
            // dispatch({type:"USER",payload:data.user})
            M.toast({
              html: " Data Posted successfully",
              classes: "#4CAF50 green"
            });
            //history.push("/");
          }
        })
        .catch(error => {
          console.log(error);
        });
    };

    return (
        <div>
            <NavBar></NavBar>
            <div className="contact-us">
                <div className="contact-us-content w-container">
                    <div className="contact-heading">
                        <h2 className="heading-5 heading-contact">Let’s do something viral and fun</h2>
                        <div className="text-block-4">Fill in the form OR <span className="contact-span">Send us an email</span></div>
                    </div>
                    <div >
                        <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Name*"
                  onChange={e => setName(e.target.value)}
                  value={name}
                />
              </div>
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Phone Number*"
                  onChange={e => setNumber(e.target.value)}
                  value={number}
                />
              </div>
              <div className="form-group bg-white">
                <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Email*"
                  onChange={e => setEmail(e.target.value)}
                  value={email}
                />
              </div>
              <div class="form-group">
              <input
                  type="text"
                  style={{ height: "50px" }}
                  className="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="  Email*"
                  onChange={e => setTxt(e.target.value)}
                  value={txt}
                />
              </div>
              <br></br>
              <center>
                <button
                  type="submit"
                  className="btn btn-dark btn-lg"
                  onClick={() => PostData()}
                >
                  Submit
                </button>
              </center>

                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
           
        </div>
    )
}

export default Contact