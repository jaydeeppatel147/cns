import React from 'react'
import NavBar from '../../Navbar'
import Footer from '../UserFooter'


const Agromatic = () => {
    return (
        <div>
            <NavBar></NavBar>
            <div className="container">
                <div className="">
                    <div className="containerci w-containerci">
                        <div className="div-block-2ci">
                            <h1>Agromatic</h1>
                            <div>Package & Brochure Designing</div>
                        </div><br></br><br></br>
                        <div className="div-blockci">
                            <p className="paragraphci">Agromatic is a pioneer in making Swiss quality products and high-tech monitoring equipment used in the grain and food industry.<br></br><br></br>What we do: Agromatic wanted to tap into their full potential and audience. We redesigned their package and made a brochure, which will appeal & resonate with their target community.

</p>
                        </div>
                    </div>

                    <div className="rowj">
                    <div className="columnj">
                        <img src="images/agromatic/1.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/agromatic/2.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        

                    </div>
                    <div className="columnj">
                        <img src="images/agromatic/3.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/agromatic/4.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        

                    </div>
                    <div className="columnj">
                        <img src="images/agromatic/5.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/agromatic/6.jpg" className="m-3 p-3" style={{width:'100%'}} />
                       

                    </div>
                    <div className="columnj">
                        <img src="images/agromatic/7.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/agromatic/8.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        

                    </div>
                    
                </div>

                </div>
                
            </div>
            <div style={{marginTop: '0%'}}>
            <Footer></Footer>
            </div>
            
        </div>
    )
}

export default Agromatic