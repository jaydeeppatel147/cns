import React from 'react'
import { Link, useHistory } from "react-router-dom";

const UserFooter = () => {
    return (
        <div>
            {/* footer */}
            <div className="footer" >
                <div className="footer-div">
                    <div className="w-row">
                        <div className="column w-col w-col-3"><img src="images/LogoCNS.png" loading="lazy" width="150" alt="" className="footerlogo" /></div>
                        <div className="column-2 w-col w-col-9">
                            <div className="nested-columns w-row">
                                <div className="w-col w-col-3">
                                    <div className="footer-grid-div">
                                        <h3 className="footer-heading">Company</h3>
                                        <Link to="/user-aboutus" style={{ textDecoration: 'none' }}>About Us</Link>
                                        <br></br><br></br>

                                        <a data-toggle="modal" data-target="#exampleModal">
                                            Privacy
                                        </a>
                                        <br></br><br></br>

                                        <Link to="/user-career" style={{ textDecoration: 'none' }}>Career</Link>


                                        <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div className="modal-dialog" role="document">
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div className="modal-body">
                                                        Chalksnslate values the privacy of its customers. We do not use or disclose information about your visits or any information you may give us, such as your name, address, e-mail identity, or telephone number, to any third parties. We do not disclose any personal information to advertisers for marketing and promotional purposes that could be used to personally identify you. This includes your password and other confidential information.
                                                        We are not responsible for any errors, omissions or representations on any of our pages or on any links on any of our pages. We do not endorse in anyway any advertisers on our web pages. Please verify the veracity of all information on your own before undertaking any purchase, or any transaction, commercial or otherwise with any of the advertisers. The linked sites are not under our control and we are not responsible for the contents of any linked site or any link contained in a linked site, or any changes or updates to such sites. We are providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by us of the site. We hereby expressly disclaim any implied warranties imputed by the laws of any jurisdiction. We consider ourselves and intend to be subject to the jurisdiction only of the Indian law. We reserve the right to make changes to our site and these disclaimers, terms, and conditions at any time
                                                     </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="w-col w-col-3">
                                    <div className="footer-grid-div">
                                        <h3 className="footer-heading">Community</h3>
                                    </div>
                                </div>
                                <div className="w-col w-col-3">
                                    <div className="footer-grid-div">
                                        <h3 className="footer-heading">Resources</h3>
                                        <Link to="/user-blog" style={{ textDecoration: 'none' }}>Blog</Link>
                                        <a href="#" className="link-2" />Webinar
                <a href="#" className="link-2" />Media
              </div>
                                </div>
                                <div className="w-col w-col-3">
                                    <div className="footer-grid-div">
                                        <h3 className="footer-heading">On Social</h3>
                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://www.instagram.com/chalksnslate_media/" className="link-block w-inline-block" ><img src="images/instagram.png" loading="lazy" width="24" alt="" className="icon socialicons" />
                                            <h5 className="heading-6">Instagram</h5>
                                        </a>

                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://www.youtube.com/channel/UCTaWejLHHW8L5aG1IxQyWCw?guided_help_flow=5" className="link-block w-inline-block" ><img src="images/youtube-fill.png" loading="lazy" width="24" alt="" class="icon socialicons" />
                                            <h5 class="heading-6">Youtube</h5>
                                        </a>

                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://www.linkedin.com/company/chalksnslate/?viewAsMember=true" className="link-block w-inline-block" ><img src="images/linkedin.png" loading="lazy" width="24" alt="" class="icon socialicons" />
                                            <h5 className="heading-6">linkedIn</h5>
                                        </a>

                                        <a target="_blank" style={{ textDecoration: 'none' }} href="https://twitter.com/chalksnslate" className="link-block social-icons-footer w-inline-block" ><img src="images/twitter-fill.png" loading="lazy" width="24" alt="" class="icon socialicons" />
                                            <h5 className="heading-6">Twitter</h5>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UserFooter