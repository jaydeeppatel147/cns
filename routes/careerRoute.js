var express = require("express");
const mongoose = require("mongoose");
var career_data = require("../models/career");
var career_Route = express.Router();

//ALL_CAREER
career_Route.get("/career_Data", (req, res) => {
    career_data
    .find()
    .then(career_data => {
      res.status(200).json({ career_data });
    })
    .catch(error => {
      console.log(error);
    });
});

//CREATED_CAREER
career_Route.post("/career", (req, res) => {
  var { name, number, email, like_to_do, upload_CV,  portfolio, message } = req.body;
  console.log(name, number, email);
  var career_Details = new career_data({
    name,
    number,
    email,
    like_to_do,
    upload_CV,
    portfolio,
    message
  });
  career_Details
    .save()
    .then(result => {
      console.log("career data", result);
      res.json({ career_data: result });
    })
    .catch(error => {
      console.log(error);
    });
});
module.exports = career_Route;