import React from 'react'
import UserFooter from '../UserFooter'
import NavBar from "../../Navbar";

const UserWD = () => {
    return (
        <div>
            <NavBar></NavBar><br></br>
            <div className="branding-2">

                {/* main img */}
                {/* <div className="branding-img">  
                    <img src="css2/images/Website_thumbnails-01_1.png" loading="lazy" sizes="(max-width: 1920px) 100vw, 1920px" srcset="images/Website-thumbnails-01-1-p-500.png 500w, images/Website-thumbnails-01-1.png 1920w" alt="" />
                </div> */}

                <div class="containerImg">
                    <img src="images/branding1.jpg" alt="Snow" sizes="(max-width: 1920px) 100vw, 1920px" />

                    <div class="centeredImg">

                        <div className="branding2">
                            <br></br><br></br>
                            <b className="textImg">Website Design</b>
                        </div>


                    </div>
                </div>

                <div className="branding-container w-container">
                    <div className="w-layout-grid grid-12">
                        <div>
                            <h1 className="heading-24">What we do</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">Services</h2>
                            <p className="paragraph-5">We come up with cutting-edge website designs, which are sure to impress those who visit it. We ideate, design and develop the entire website, so you don’t have to worry about it. </p>
                            <h2 className="heading-25">Roles &amp; responsibility</h2>
                            <p className="paragraph-5">Right from making your website unique & stand-out from your competitors, to designing, developing and making it live, CNS has got you covered. </p>
                        </div>
                        <div>
                            <h1 className="heading-24">Process</h1>
                        </div>
                        <div>
                            <h2 className="heading-25">How we do things</h2>
                            <p className="paragraph-5"> CNS’ creative UI/UX designers will come up with a layout which makes your website appealing to your audience. We also use cutting edge features to impress the crowd the website will attract. Post this, we develop the website, make it live. You can sit back & relax. We do all the hard work, you watch the clicks pour in! 
</p>
                        </div>
                    </div>
                </div>

                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default UserWD