import React from 'react'
import UserFooter from './UserFooter'
// import UserHeader from './UserHeader'
import MiniContactForm from './MiniContactForm'
import { Link, useHistory } from "react-router-dom";
import animationData from './lottie2/goat_jump.json';
import Lottie from 'react-lottie';
import NavBar from "../Navbar";

const AboutUs = () => {

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    };

    return (
        <div>
            <NavBar></NavBar>
            <div className="body-2">

                {/* about us image */}
                <div className="about-us">
                    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" className="navbar-2 aboutus-nav w-nav">

                    </div><br></br>
                    <div className="box">
                        <div className="aboutus-tagline">We’re the ones who think out-of-the-box solutions to pressing issues that spot your brand from reaching its full potential. We take risks, so you don’t have to worry about it.
</div>
                    </div>
                </div>

                {/* contant */}
                <div className="about-us-2">
                    <div className="aboutus-div">
                        <div className="w-layout-grid grid-5">
                            <div className="content-block">
                                <h2 className="about-us-heading">6 Mo of innovative solutions
</h2>
                                <p className="about-us-para">We take up challenging branding issues and provide ingenious solutions to make your brand stand out among the rest. Our dedicated team ensures that we exceed your expectations.
</p>
                            </div>
                            <div className="content-block">
                                <h2 className="about-us-heading">6 Mo of perseverance</h2>
                                <p className="about-us-para">We know that the best things take time & a little bit of luck. But, luck favours those who are bold and persist in their endeavors. We are relentless in our pursuit of excellence.</p>
                            </div>
                            <div className="content-block">
                                <h2 className="about-us-heading">Growing team of 20+ youngsters</h2>
                                <p className="about-us-para">Our glowing team of youngsters are versatile & highly experienced. We are a powerhouse, which strives to establish a unique identity in the digital marketing world.</p>
                            </div>
                        </div><img src="images/fire.png" loading="lazy" alt="" className="lit" />
                    </div>
                    <div className="approach">
                        <h1 className="approach-heading">Our Approach for a successful brand experience</h1>
                        <div className="w-layout-grid grid-6">
                            <div id="w-node-f78cc2ddcbd3-b59c0f72" className="grid-div"><img src="images/understand.png" loading="lazy" alt="" className="understand" />
                                <h3 className="heading-9">UNDERSTAND</h3>
                                <p className="about-us-para">We understand that every brand’s uniqueness needs to be expressed. We ensure that our strategies are suited to exactly what your brand needs.</p>
                            </div>
                            <div id="w-node-f78cc2ddcbd9-b59c0f72" className="grid-div"><img src="images/brainstrom.png" loading="lazy" alt="" className="brainstorm" />
                                <h3 className="heading-9">BRAINSTORM</h3>
                                <p className="about-us-para">At CNS, we come up with the most innovative & out-of-the-box ideas, which will ensure maximum profit for your brand too. We churn out dynamic ideas, which are also easy to execute too.
</p>
                            </div>
                            <div id="w-node-f78cc2ddcbdf-b59c0f72" className="grid-div"><img src="images/design.png" loading="lazy" alt="" className="design" />
                                <h3 className="heading-9">DESIGN</h3>
                                <p className="about-us-para">Our team of talented & experienced designers come up with innovative creatives which you will never have the heart to reject. Creativity is at the heart of everything we do at CNS.</p>
                            </div>
                            <div id="w-node-f78cc2ddcbe5-b59c0f72" className="grid-div"><img src="images/improve.png" loading="lazy" alt="" className="improve" />
                                <h3 class="heading-9">IMPROVE</h3>
                                <p class="about-us-para">Our goal at CNS is to improvise, adapt & overcome. We know, and transcend current market trends in order to shape the future according to your requirements.
</p>
                            </div>
                        </div>
                    </div>
                </div>


                {/* Second contant */}
                <div className="about-us-2">
                    <div className="_3-div">
                        <div className="w-row">
                            <div className="column-7 w-col w-col-5 w-col-stack w-col-small-small-stack">
                                <img src="images/bottal1.png" loading="lazy" alt="" />
                            </div>
                            <div className="column-7 w-col w-col-7 w-col-stack w-col-small-small-stack">
                                <div className="solution">
                                    <h1 className="solution-heading">We are the people bold enough to do viral things with your brand</h1>
                                    <div className="list">
                                        <div className="w-row">
                                            <div className="column-9 w-col w-col-4">
                                                <div className="list-div">
                                                    <h4 className="list-heading">STRATEGY</h4>
                                                    <div className="line"></div>
                                                    <a href="#" className="link-3">Brand Strategy</a>
                                                    <a href="#" className="link-3">UI/UX Audit</a>
                                                    <a href="#" className="link-3">Consulting</a>
                                                    <a href="#" className="link-3">Brand Communication</a>
                                                </div>
                                            </div>
                                            <div className="column-9 w-col w-col-4">
                                                <div className="list-div">
                                                    <h4 className="list-heading">DESIGN</h4>
                                                    <div className="line"></div>
                                                    <a href="#" className="link-3">Logo Design</a>
                                                    <a href="#" className="link-3">Branding and Style guides</a>
                                                    <a href="#" className="link-3">Packaging design</a>
                                                    <a href="#" className="link-3">Graphics and Social Media</a>
                                                    <a href="#" className="link-3">UI Design</a>
                                                    <a href="#" className="link-3">UX Design</a>
                                                    <a href="#" className="link-3">Website Design</a>
                                                    <a href="#" className="link-3">Motion Graphics</a>
                                                    <a href="#" className="link-3">Website Development</a>
                                                </div>
                                            </div>
                                            <div className="column-9 w-col w-col-4">
                                                <div className="list-div">
                                                    <h4 className="list-heading">DIGITAL</h4>
                                                    <div className="line"></div>
                                                    <a href="#" className="link-3">Brand Strategy</a>
                                                    <a href="#" className="link-3">UI/UX Audit</a>
                                                    <a href="#" className="link-3">Consulting</a>
                                                    <a href="#" className="link-3">Packaging design</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="_4-div">
                        <div className="w-container">
                            <h1 className="heading-12">We’re also working on a community</h1>
                        </div>
                        <div className="w-layout-grid grid-7">
                            <div className="top-grid">
                                <div className="w-layout-grid grid-8">

                                    <div className="card grad boxshadow" >
                                        <a href="https://www.instagram.com/chalksnslate_media/" target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">
                                            <img src="images/instagram.png" loading="lazy" alt="" class="image-3" />
                                            <div className="text-block-7">Instagram is explosive, and CNS lights the fuse
                                            with posts that bring out our team’s creativity and ingenuity.
                                    </div>
                                        </a>
                                    </div>


                                    <div className="card grad boxshadow">
                                        <a href='https://www.linkedin.com/company/chalksnslate/?viewAsMember=true' target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">

                                            <img src="images/linkedin.png" loading="lazy" alt="" className="image-3" />
                                            <div className="text-block-7">We value our resources, and CNS ensures that those who join our team are valued & appreciated for their contributions.</div>
                                        </a>
                                    </div>

                                    <div className="card grad boxshadow">
                                        <a href=" https://www.youtube.com/channel/UCTaWejLHHW8L5aG1IxQyWCw?guided_help_flow=5" target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">

                                            <img src="images/youtube.png" loading="lazy" alt="" className="image-3" />
                                            <div className="text-block-7"><br></br>With over 10 YouTube videos, we are creating a community that shares & identifies with our educational content.</div>
                                        </a>
                                    </div>

                                    <div className="card grad boxshadow">
                                        <a href="https://medium.com/chalksnslate-media" target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">

                                            <img src="images/medium.png" loading="lazy" alt="" className="image-3" />
                                            <div className="text-block-7"> We endeavor to build a set of talented writers who are creative, incurable wordsmiths & enjoy inking their thoughts.</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="bot-grid">
                                <div className="w-layout-grid grid-9">
                                    <div className="card grad boxshadow">
                                        <a href="https://twitter.com/chalksnslate" target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">

                                            <img src="images/twitter.png" loading="lazy" alt="" className="image-3" />
                                            <div className="text-block-7"><br></br>CNS breaks conventions and expresses out-of-the-box ideas which will make heads turn, stare, wonder and admire our work</div>
                                        </a>
                                    </div>

                                    <div className="card grad boxshadow">
                                        <a href="https://www.behance.net/chalksnslate" target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">

                                            <img src="images/behance.png" loading="lazy" alt="" className="image-3" />
                                            <div className="text-block-7"><br></br>Our team of talented designers have a dynamic & versatile portfolio, which ranges from animation to website designing.
                                    </div>
                                        </a>
                                    </div>

                                    <div className="card grad boxshadow">
                                        <a href="https://dribbble.com/Chalksnslate" target="_blank" style={{ textDecoration: 'none' }} rel="noopener noreferrer">

                                            <img src="images/d1.png" loading="lazy" alt="" className="image-3" />
                                            <div className="text-block-7"> CNS develops graphics & illustrations which are highly intuitive & customizable, making others’ jaw drop in awe of our creatives.
                                    </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><br></br>
                    <div className="_5-div">






                        <Lottie style={{ margin: '0px 0 8% 15%' }}
                            options={defaultOptions}
                            className='lottie'
                            height={200}
                            width={400}
                        /><br></br>
                        <div>
                            <center>
                                <Link to="/user-work" style={{ margin: '0 10% 0 -270%' }}>
                                    <div className="button-5 w-button">See our Projects</div>
                                </Link>
                            </center>
                        </div>
                    </div>
                </div><br /><br />




                <MiniContactForm></MiniContactForm>
                <UserFooter></UserFooter>
            </div>
        </div>
    )
}

export default AboutUs