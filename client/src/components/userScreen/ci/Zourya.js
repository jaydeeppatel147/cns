import React from 'react'
import NavBar from '../../Navbar'
import Footer from '../UserFooter'


const Zourya = () => {
    return (
        <div>
            <NavBar></NavBar>
            <div className="container">
                <div className="">
                    <div className="containerci w-containerci">
                        <div className="div-block-2ci">
                            <h1>Zaurya</h1>
                            <div>Website Development & Social Media management</div>
                        </div><br></br><br></br>
                        <div className="div-blockci">
                            <p className="paragraphci">Zaurya Realtech Private Limited is an upcoming real-estate company involved in Real estate activities specifically community development and buying, selling & renting property.


                            <br></br>
                            What we do: CNS designed & created a website for Zaurya. We also strategized and formed a social media calendar to target audiences specific to developing curated communities.






</p>
                        </div>
                    </div>

                    <div className="rowj">
                    <div className="columnj">
                        <img src="images/zourya/1.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/zourya/7.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        

                    </div>
                    <div className="columnj">
                        <img src="images/zourya/3.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/zourya/4.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        

                    </div>
                    <div className="columnj">
                        <img src="images/zourya/5.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        <img src="images/zourya/6.jpg" className="m-3 p-3" style={{width:'100%'}} />
                       

                    </div>
                    <div className="columnj">
                        <img src="images/zourya/2.jpg" className="m-3 p-3" style={{width:'100%'}} />
                        {/* <img src="images/agromatic/8.jpg" className="m-3 p-3" style={{width:'100%'}} /> */}
                        

                    </div>
                    
                </div>

                </div>
                
            </div>
            <div style={{marginTop: '0%'}}>
            <Footer></Footer>
            </div>
            
        </div>
    )
}

export default Zourya