import React, { Component, useState, useEffect, useContext } from "react";
// import { UserContext } from "../../App";
import { Link } from "react-router-dom";
import M from "materialize-css";
import NavBar from '../../Navbar'
import UserFooter from '../UserFooter'

const Blog_Internal = () => {
  return (
    <div>
    <NavBar></NavBar><br></br><br></br>
    <div data-ix="page-loading" className="page-wrappersidebar-headerh1">
      <div className="post-header-sectionh1">
        <div className="wrapperh1 w-containerh1">
          <div className="post-header-wrapper">
            <a href="#" data-ix="fade-up-1" className="categoryh1">
              {/* DESIGN */}
            </a>
            <h1 data-ix="fade-up-2">
              {/* When you are down and out, how to <br />
              keep going */}
            </h1>
            <a
              href="#"
              data-ix="fade-up-3"
              className="post-header-authorh1 w-inline-blockh1"
            >
              <div>Post by</div>
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-header-avatarh1"
              />
              <div>Divyansh</div>
            </a>
          </div>
        </div>
        <div className="post-header-overlayh1"></div>
      </div>
      <div className="sectionh1">
        <div className="wrapper w-containerh1">
          <div className="blog-post-contenth1">
            <div className="blog-post w-richtexth1">
              <h2>
                <b>CULT BRANDING</b>
              </h2>
              <p>
                A cult is a group of people who have a common interest - be it
                religious, spiritual, a philosophical belief pertaining to a
                particular object, personality or a goal. When we combine the
                term branding with it, this applies to a group of customers who
                are loyal to a certain corporate identity, and aggressively
                follow it. They associate themselves with the brand logo,
                tagline or overall brand image. It is beneficial to the brand as
                it encourages the entry of new customers, resulting in a larger
                customer base, since existing loyal customers would persuade
                their family and friends to indulge in the same.
              </p>
              <br />
              <p>
                Cult branding is not to be confused with a fad - which is a
                temporary trend that eventually fades out, like the fidget
                spinner, that quickly became so popular and everybody had it. A
                good example of a cult brand is IKEA, whose customers cannot
                think of any other brand for their furniture.
              </p>

              <br></br>
              <p>
                Every corporate identity cannot have a cult branding situation.
                A brand must offer something unique to its customers, maybe a
                powerful origin story, or something memorable and impactful to
                their customers over the long term. People choose to be a part
                of a cult due to the following reasons:
              </p>

              <ol>
                <li>
                  Status - The iPhone, for example, shows a person’s status and
                  is regarded as a class symbol. The brand logo itself is so
                  powerful that people usually don’t hide it - therefore buying
                  transparent phone covers.
                </li>
                <li>
                  To be part of the family - People might choose a brand to
                  become a member of the family. If you buy a Harley Davidson
                  motorcycle, you are not just getting a bike, but you are also
                  getting a membership of the club.{" "}
                </li>
                <li>
                  Emotional connect - Arthur Conan Doyle, author of the
                  eternally famous Sherlock Holmes books, had killed his primary
                  character in 1893 after having written multiple stories,
                  citing his boredom as a reason to end the series. Loyal
                  readers were so hooked with the adventures of Sherlock Holmes
                  that they insisted on bringing him back, after which he was
                  resurrected in 1905, almost twelve years later. The series has
                  resulted in a cult following over the years with multiple
                  cartoons, TV shows, movies, merchandise and a dedicated museum
                  which was established in 1990.
                </li>
                <li>
                  Share a common belief - People want to experience a sense of
                  belonging from a brand that they associate themselves with.
                  The values and beliefs of the brand become part of their own
                  personal identity. For example, there will always be a
                  community of individuals who prefer Coca Cola over Pepsi, or
                  vice versa.
                </li>
              </ol>
              <p>
                Cult brands regard their customers as royalty. They try to
                understand the customer’s psychology, their desires, and what
                would motivate them to indulge in the purchase of their unique
                product or service. They establish a relationship which doesn’t
                stop at the sale of a product, but is maintained even after in
                the form of services which are given equal importance. That is
                what differentiates them from others, making their customer
                believe they are an important part of it.
              </p>
              <br />
              <p>
                Cult following is not only confined to a product, but can also
                be ascribed to a certain personality, like the Kardashians.
                People stay updated on everything they do, wanting to buy
                clothing or makeup products exclusively launched by members of
                the family. Sometimes, the logo of a brand is so impactful that
                people will buy anything, for example, Dove or Nike. A corporate
                identity, in order to be a cult, should examine existing ones to
                discover what transforms fondness into fanaticism. Cult brands
                grow slowly and organically, with the enthusiasm of their
                customer base. There are many things that help a brand to gain a
                cult status. The difference is made with the risk they take to
                experiment on new and innovative ideas. They sell more than a
                product or service; they are selling experiences and a story
                that grabs people’s attention.
              </p>
              <br />
              <h4>What can we learn from existing cult brands?</h4>
              <br />

              <ul>
                <li>
                  VANS, as a brand today, stands as an alternative sport symbol
                  for snowboarding and skateboarding. It sponsors and organises
                  sporting events each year with enthusiastic customers and
                  renowned adventurers, which is the best way to connect with
                  them and stay inclusive. They also build and operate
                  skateboarding parks to make their customers understand they
                  care for them.
                </li>
                <li>
                  Amazon's main focus is to satisfy their customers by
                  constantly seeking their feedback in the form of rating and
                  reviews, they pride in fast shipment of goods, easy
                  replacement, as well as premium variety in choice of
                  entertainment (film, tv, music). Basically, the brand makes a
                  customer believe that it is their one-stop shop for anything
                  they need.
                </li>
              </ul>
              <p>
                The real deal to cult branding is to focus on customers, making
                them a part of the overall corporate identity. Brands must sell
                products that grab people’s attention immediately, being
                confident, focused and authentic.
              </p>
              <br />
            </div>
            <div className="post-date-wrapperh1">
              <div>Published</div>
              <div className="post-dateh1">on 5th Dec</div>
            </div>
            <div className="post-author-profileh1">
              <img
                src="https://d3e54v103j8qbb.cloudfront.net/plugins/Basic/assets/placeholder.60f9b1840c.svg"
                alt=""
                className="post-author-avatarh1"
              />
              <div className="post-author-infoh1">
                <h5 className="post-author-nameh1">Divyansh</h5>
                <div className="post-author-bioh1">
                  This is some text inside of a div block.
                </div>
                <a href="#" className="post-author-linkh1">
                  View All Posts
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="section grey-sectionh1">
        <div className="section-header-wrapperh1">
          <div className="section-small-headerh1">More From Blog</div>
          <h2 className="section-headerh1">You Might Also Like</h2>
        </div>
        <div className="wrapper w-containerh1">
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
          <div className="div-block-2h1">
            <a
              href="#"
              className="post-card-v2-image-wrapperh1 w-inline-blockh1"
            >
              <img
                sizes="(max-width: 479px) 79vw, (max-width: 767px) 24vw, (max-width: 991px) 26vw, (max-width: 5785px) 28vw, 1620px"
                srcset="images/john-towner-128480-p-500.jpeg 500w, images/john-towner-128480-p-1080.jpeg 1080w, images/john-towner-128480-p-1600.jpeg 1600w, images/john-towner-128480.jpg 1620w"
                src="images/john-towner-128480.jpg"
                alt=""
                className="zoom-on-hoverh1"
              />
            </a>
            <div className="categoryh1 post-card-v2-categoryh1">marketing</div>
            <h5 className="post-card-v2-headerh1">How to make websites</h5>
            <a href="#" className="post-card-v2-linkh1">
              Read More
            </a>
          </div>
        </div>
      </div> */}
    </div>
    <UserFooter></UserFooter>
    </div>
  );
};

export default Blog_Internal;
