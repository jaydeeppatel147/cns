var express = require("express");
const mongoose = require("mongoose");
var contact_data = require("../models/contact");
var contact_Route = express.Router();

//ALL_CONTACT
contact_Route.get("/contact_Us_Data", (req, res) => {
  contact_data
    .find()
    .then(contact_datas => {
      res.status(200).json({ contact_datas });
    })
    .catch(error => {
      console.log(error);
    });
});

//CREATED_CONTACT
contact_Route.post("/contact_Us", (req, res) => {
    var { name, number, email, txt } = req.body;
    console.log(name, number, email, txt);
    if (!number || !email) {
      return res.status(422).json({ error: "PLEASE ADD ALL NUMBER AND EMAIL" });
    }
    var contact_Details = new contact_data({
      name,
      number,
      email,
      txt
    });
    contact_Details
      .save()
      .then(result => {
        console.log("contact data", result);
        res.json({ contact_data_user: result });
      })
      .catch(error => {
        console.log(error);
      });
  });

module.exports = contact_Route;